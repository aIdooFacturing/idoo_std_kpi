$(function(){
	//$("#title_right").html("두산 협력업체");
	getComName();
	bindEvt();
	drawFlag();
	$("#content_table").css("color" , "white")
});

function cancelBanner(){
	window.localStorage.setItem("banner", "false");
	location.reload();
};

function getBanner(){
	var url = ctxPath + "/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100); 
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			//twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = false;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 300)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},8000 * 2, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};

function chkBanner() {
	if (window.localStorage.getItem("banner") == "true") {
		getBanner();
		$("#intro_back").css({
			"display" : "block",
			"pointer-events" : "none"
		});
	} else {
		$("#intro").html("");
		$("#intro_back").css("display", "none");
	}
};

function drawFlag(){
	var ko = "<img src=" + ctxPath + "/images/ko.png id='ko' class='flag'/>";
	var cn = "<img src=" + ctxPath + "/images/cn.png id='cn' class='flag'/>";
	var en = "<img src=" + ctxPath + "/images/en.png id='en' class='flag'/>";
	var de = "<img src=" + ctxPath + "/images/de.png id='de' class='flag'/>";
	
	var div = "<div id='flagDiv'>" + ko 
									+ cn 
									+ en 
									+ de
									+ "</div>";
	$("body").prepend(div);
	
	$("#flagDiv").css({
		"position" : "absolute",
		"left" : marginWidth,
		"top" : marginHeight + getElSize(20)
	});
	
	$(".flag").css({
		"width" : getElSize(100),
		"cursor" : "pointer"
	}).click(changeLang)
};
function bindEvt(){
};

function changeLang(){
	var lang = this.id;
	var url = window.location.href;
	var param = "?lang=" + lang;
	
	if(url.indexOf("fromDashBoard")!=-1){
		param = "&lang=" + lang;
		if(url.indexOf("&lang")!=-1){
			url = url.substr(0, url.lastIndexOf("&lang"));
		}
	}else{
		url = url.substr(0, url.lastIndexOf("?"))
	}
	
	location.href = url + param;
}

function decode(str){
	return decodeURIComponent(str).replace(/\+/gi, " ")
};

function getComName(){
	var url = ctxPath + "/chart/getComName.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			$("#title_right").html(decode(data));
		}
	});
};

function addZero(str){
	if(str.length==1) str = "0" + str;
	return str;
}

function showCorver(){
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

function closeCorver(){
	$("#corver").css({
		"z-index" : -999,
	});
}