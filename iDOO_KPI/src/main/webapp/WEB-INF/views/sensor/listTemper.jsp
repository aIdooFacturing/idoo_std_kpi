<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<thead>
	<tr>
		<th>NO</th>
		<th>센서명</th>
		<th>수집시간</th>
		<th>온도</th>
		<th>기록시간</th>
	</tr>
</thead>

<tfoot>
	<tr>
		<th>NO</th>
		<th>센서명</th>
		<th>수집시간</th>
		<th>온도</th>
		<th>기록시간</th>
	</tr>
</tfoot>

<tbody>
<c:choose>
	<c:when test="${!empty listTemper }">
		<c:forEach var="temper" items="${listTemper }" varStatus="status">
			<tr>
				<td>${status.count}</td>
				<td>${temper.temperId }</td>
				<td>${temper.temperDatetime }</td>
				<td>${temper.temperValue}</td>
				<td>${temper.regdt }</td>
			</tr>			
		</c:forEach>
	</c:when>
		
	<c:otherwise>
	</c:otherwise>
</c:choose>
</tbody>