<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/WEB-INF/views/include/unomiclib.jsp"%>
<%@ page import="com.unomic.dulink.common.domain.CommonCode"%>
<%@ page import="com.unomic.dulink.chart.domain.*"%>
<%@ page session = "true" %>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common.min.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.silver.min.css"/>
<script src="https://kendo.cdn.telerik.com/2017.3.1026/js/kendo.all.min.js"></script>
</head>


<style>
body{
	overflow: hidden;
}

</style>
<script type="text/javascript">

/* 	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight; */

	var nm = '<%=(String)session.getAttribute("nm")%>';
	nm = decode(nm);
	var empCd = '<%=(String)session.getAttribute("empCd")%>';

	var ip;
	var popId;
	var line;
	var test;
	$(function(){
		//전체적인 기본 CSS
		defulatCss();
		
		//POP 명 가져오기
		popLineChk();

		//공지사항 가지고오기
		noticeChk();
		// 5분 마다 SESSION 로그인 체크
		setInterval(function() {
			sessionChk() 
		}, 300000);
	})
	
	function defulatCss(){
//		console.log($("#header").height())
		// 제일 마지막으로 CSS적용 시키기 위해
		setTimeout(function() {
			
			$("#noticeMsg").css({
				"float" : "left",
				"top" : $("#header").height(),
				"height" : originHeight * 0.11,
				"width" : originWidth,
//				"background" : "red"
			})
			
			$("#backIcon").css({
				"font-size": $("#logo2").height() *  0.7
			})
			
		}, 1);
	}


	//사용자 session Chk 
	function sessionChk(){
		nm = '<%=(String)session.getAttribute("nm")%>';
		nm = decode(nm);
		empCd = '<%=(String)session.getAttribute("empCd")%>';
		
		if(empCd==null || empCd=="null"){
			location.href='${ctxPath}/pop/popIndex.do';
		}
		$("#userId").html(nm);
	}
	
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	//라인 글자 CSS 적용시키기
	function lineCss(){
		$("#popName").css({
//			"top" : originHeight,
//			"left" : originWidth,
			"color" : "black",
			"font-size" : originHeight * 0.05,
//		    "font-weight": "bold",
	        "font-family": "fantasy"
		})
		
		setTimeout(function() {
			$("#popName").css({
				"top" : originHeight - $("#popName").height() * 1.20,
				"left" : originWidth * 0.015
//				"left" : originWidth - $("#popName").width() * 1.15
			})
		}, 1);
		
	}
	
	//공지사항 확인하기
	function noticeChk(){
		
		chk = false;
		text = "나는 공지사항이다. 공지사항 공지사항";
		color = "rgb(245, 190, 220) none repeat scroll 0% 0% / auto padding-box border-box";
		if(chk){
			$(".noticetext").html(text);
			
			$(".noticetext").css({
				"font-size" : $("#logo2").height() *  0.8
				,"background" : color
			})
		}
	}
	//pop 장비 확인하기
	function popLineChk(){
	    var head = document.getElementsByTagName('head')[0];
	    var script= document.createElement('script');

	    window.getIP = function(json) {
	    	//외부 IP
	        public_ip = json.ip;

	        //내부(사설 IP) 가져오기
	        getPrivateIP(function(ip){
	            private_ip = ip

	           
                var url = "${ctxPath}/pop/popLineChk.do"
				var param = "ip=" + private_ip;
                

                console.log(param)
                $.ajax({
                    url : url,
                    data : param,
                    type : "post",
                    dataType : "json",
                    success : function(data){
                    	
                    	ip = data.ip;
        				popId = data.popId;
        				line = data.line;
        				
        				test = data.pregDate;
        				
        				if(line==null || line==undefined){
	        				$("#popName").html("KIOSK 등록 필요");
        				}else{
	        				$("#popName").html(popId + ". " + decode(line));
        				}
        				//popName Css
        				lineCss();
        				
        				ready();
                    },error:function(request,status,error){
                        alert("code = "+ request.status + " message = " + request.responseText + " error = " + error); // 실패 시 처리
                    }

                })

	        });
	    };

	    script.type= 'text/javascript';
	    script.src= 'https://api.ipify.org?format=jsonp&callback=getIP';
	    head.appendChild(script);

		
		
		
		
		/* console.log("chk")
// popLineChk		

		
 		var url = "${ctxPath}/pop/popLineChk.do";
		
		$.ajax({
			url : url,
//			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				ip = data.ip;
				popId = data.popId;
				line = data.line;
				
				test = data.pregDate;
				
				$("#popName").html(decode(line))
				//popName Css
				lineCss();
				console.log(data.ip)
				console.log(data.popId)
				console.log(data.line)
			},error : function(data){
				alert("err")
			}
		})  */
	}
	
	function getPrivateIP(onNewIP) { //  onNewIp - your listener function for new IPs
	    //compatibility for firefox and chrome
	    var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
	    var pc = new myPeerConnection({
	            iceServers: []
	        }),
	        noop = function() {},
	        localIPs = {},
	        ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
	        key;

	    function iterateIP(ip) {
	        if (!localIPs[ip]) onNewIP(ip);
	        localIPs[ip] = true;
	    }

	    //create a bogus data channel
	    pc.createDataChannel("");

	    // create offer and set local description
	    pc.createOffer().then(function(sdp) {
	        sdp.sdp.split('\n').forEach(function(line) {
	            if (line.indexOf('candidate') < 0) return;
	            line.match(ipRegex).forEach(iterateIP);
	        });

	        pc.setLocalDescription(sdp, noop, noop);
	    }).catch(function(reason) {
	        // An error occurred, so handle the failure to connect
	    });

	    //listen for candidate events
	    pc.onicecandidate = function(ice) {
	        if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;
	        ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
	    };
	}
	
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
</script>
<body>

	<div id="noticeMsg" style="position: absolute;">
		<table style="width: 100%; height: 100%; text-align: center; vertical-align: middle;">
			<tr>
				<td class="noticetext">
				</td>
			</tr>
		</table>
	</div>

<!-- marquee behavior=alternate scrollamount='20' id='alarmText'>사원 바코드를 입력해주세요</marquee></span> -->
<!-- 	아래 왼쪽에 키오스크 라인명 영역 -->
	<div id="popName" style="position: absolute;">
		
	</div>
</body>
</html>