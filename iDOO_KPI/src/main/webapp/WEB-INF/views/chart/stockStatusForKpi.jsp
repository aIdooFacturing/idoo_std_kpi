<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}
button{
	margin: 0px;
	padding: 0px;
	border: none;
}
</style> 
<script type="text/javascript">
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
		
		return year + "-" + month + "-" + day;
	};
	
	var handle = 0;
	
	$(function(){
		getPrdNo();
		createNav("kpi_nav", 5);
		
		setEl();
		setDate();	
		time();
		
		setEvt();
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function setEvt(){

	};
	
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function getPrdNo(){
		var url = "${ctxPath}/common/getPrdNo.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + decode(data.prdNo) + "'>" + decode(data.prdNo) + "</option>"; 
				});
				
				$("#group").html(option);
				
				getStockStatus();
			}
		});
	};
	
	var className = "";
	var classFlag = true;

	var kendotable
	$(document).ready(function(){
		kendotable = $("#grid").kendoGrid({
			height:getElSize(1665),
			columns:[{
				field:"prdNo",title:"${prd_no}",width:getElSize(620)
			},{
				field:"oprNm",title:"${Current_process}",template:"#=decode(oprNm)#",width:getElSize(320)
			},{
				field:"spec",title:"${spec}",width:getElSize(320)
			},{
				field:"iniohdCnt",title:"${Basic_stock}",width:getElSize(320)
			},{
				field:"rcvCnt",title:"${income_cnt}",width:getElSize(320)
			},{
				field:"notiCnt",title:"${faulty_cnt}",width:getElSize(320)
			},{
				field:"issCnt",title:"${release_count}",width:getElSize(320)
			},{
				field:"cnt",title:"${stock_cnt}",width:getElSize(320)
			}]
		}).data("kendoGrid")
	});
	//태이블 table
	function getStockStatus(){
		$.showLoading();
		classFlag = true;
		var url = "${ctxPath}/chart/getStockStatus.do";
		
		var param = "prdNo=" + $("#group").val()+"&date="+moment().subtract(1,'days').format("YYYY-MM-DD");
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				console.log(json)
				var dataSource = new kendo.data.DataSource({
					  data: json
				});
//				var grid = $("#grid").data("kendoGrid");
				kendotable.setDataSource(dataSource);
				
				$("#grid thead th").css("font-size",getElSize(45));
				$("#grid tbody td").css("font-size",getElSize(43));
				
				$("#print_table").empty();
				
				var table ="<table border='1' class='top_table' width='95%' align='center'><tr>"+
				"<th rowspan='3' colspan='2'>재고 현황<br>"+ 
				"<br>"+ setDate() +"</th><th style='background-color:lightgray' colspan='3'>결제</th></tr>" +
				"<tr style='background-color:lightgray'><th>담당</th><th>검토</th><th>승인</th></tr>" +
				"<tr class='sign' ><th> </th><th> </th><th> </th></tr>" +
				"</table>"+
				"<table class='p_table' border='1' width='95%' align='center'>"+
				"<tr style='background-color:lightgray'><th>품번</th><th>현재공정</th><th>규격</th><th>기초(전일)재고</th><th>입고수량</th><th>불량수량</th><th>불출수량</th><th>재고수량</th>" ;
				
				var divideNum=38;		
				var page=1;		//page size표시
				
				var index=0;	//행 갯수 세기
				var chk=1;
				
				var totalpage=Math.ceil(chk/divideNum);		//총 페이지수
				var iniohdCnt=0
				var issCnt=0
				var notiCnt=0
				var ohdCnt=0
				var rcvCnt=0;// header부분 값 구하기
				//header 총 갯수 구하기
				$(json).each(function(idx,data){
					
					//한개의 tr
					if(idx==0){
						//작업자별 장비 갯수 구하기
						for(i=0;i<json.length;i++){
							if(data.prdNo==json[i].prdNo){
								iniohdCnt+=Number(json[i].iniohdCnt)
								issCnt+=Number(json[i].issCnt)
								notiCnt+=Number(json[i].notiCnt)
								ohdCnt+=Number(json[i].cnt)
								rcvCnt+=Number(json[i].rcvCnt)
							}
						}
						
						index++;
						table +="<tr style='background-color:lightgray' class='totalwork'><td colspan='3'>품번 : "+ data.prdNo +"</td><td>"+ iniohdCnt +"</td><td>"+ rcvCnt +"</td><td>"+ notiCnt +"</td><td>"+ issCnt +"</td><td>"+ ohdCnt +"</td>";
					}else{	
						iniohdCnt=0
						issCnt=0
						notiCnt=0
						ohdCnt=0
						rcvCnt=0
						
						for(i=0;i<json.length;i++){
							if(data.prdNo==json[i].prdNo){
								iniohdCnt+=Number(json[i].iniohdCnt)
								issCnt+=Number(json[i].issCnt)
								notiCnt+=Number(json[i].notiCnt)
								ohdCnt+=Number(json[i].cnt)
								rcvCnt+=Number(json[i].rcvCnt)
							}
						}
						
						index++;
						if(json[idx].prdNo!=json[idx-1].prdNo){
							index++;
							table +="<tr style='background-color:lightgray' class='totalwork'><td colspan='3'>품번 : "+ data.prdNo +"</td><td>"+ iniohdCnt +"</td><td>"+ rcvCnt +"</td><td>"+ notiCnt +"</td><td>"+ issCnt +"</td><td>"+ ohdCnt +"</td>";
						}
					}
					
					//tr
					table +="<tr><td>" + data.prdNo + "</td><td>" +	decode(data.oprNm) + "</td><td>" + data.spec + "</td><td>" + data.iniohdCnt + "</td>"+
							"<td>" + data.rcvCnt+"</td><td>"+ data.notiCnt+"</td><td>"+ data.issCnt+"</td><td>"+ Number(data.cnt) +"</td>";
					
					//a4 size page 넘기기
					if(idx!=0 && index>divideNum ){	
						page++;
						index=1;
						table +="</table>" +
								"<div style='page-break-before:always'></div>";
								
						table +="<br><br><br><br> <table class='p_table' border='5' width='95%' align='center'><tr class='sign'><th colspan='7' rowspan='2'>재고 현황 <br>"+setDate()+
								"</th>	<th style='background-color:lightgray' colspan='2'>페이지</th></tr>" +
								"<tr class='sign' ><th colspan='2'>" + page +" / " + totalpage +"</th></tr>" +
								"<tr style='background-color:lightgray'><th>품번</th><th>현재공정</th><th>규격</th><th>기초(전일)재고</th><th>입고수량</th><th>불량수량</th><th>불출수량</th><th>재고수량</th>" ;
								
					}
				})
			
				table +="</table>"
				$("#print_table").append(table);
				
				
				/* tr += "</tbody>"; */
				
				$(".p_table tr th").css("font-size","11px")
				$(".p_table tr td").css("font-size","11px")
				
				$(".sign").css({
					"height" : "50px"
				})
				$(".sign th").css({
					"width" : "70px"
				})

				$("button, input[type='time']").css("font-size", getElSize(40));
				
				$("body, html").css({
					"overflow" : "hidden"	
				})
				$(".top_table").css({
					"font-size" : "10px"
				})
				$.hideLoading()
			}
		});
	};
	
	function pop_print(){
		win = window.open();
        self.focus();
        win.document.open();
        win.document.write('<'+'html'+'><'+'head'+'><'+'style'+'>');
        win.document.write('body, td { font-family: Verdana; font-size: 10pt;}');
        win.document.write('<'+'/'+'style'+'><'+'/'+'head'+'><'+'body'+'>');
        win.document.write(document.getElementById('print_table').innerHTML);
        win.document.write('<'+'/'+'body'+'><'+'/'+'html'+'>');
        win.document.close();
        win.print();
        win.close();
	}
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/kpi_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/green_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td> 
								<div>
									<%-- <div id ="groupDiv" style="border: 2px solid white; padding: 8px; margin-top:20px; display: inline;">
										<label style="background: linear-gradient( to bottom, #ECEBFF, #DAD9FF ); position: inherit; color: black; padding: 7px; font-weight: bold; border-radius: 5px;"><spring:message code="prd_no"></spring:message></label> :
										<select id="group" style="width: 200px; height: 29px; font-size: 20px;"></select>
									</div> --%>
									<div id ="groupDiv" style=" padding-top: 8px; padding-bottom: 8px; margin-top:20px; margin-left:0px; margin-right:0px; display: inline; background: white">
										<div style="background: linear-gradient( to bottom, #ECEBFF, #DAD9FF ); display: inline; color: black; margin:0px; padding:8px;font-weight: bold; "><spring:message code="prd_no"></spring:message></div>
										<select id="group" style="width: 200px; height: 30px; font-size: 20px; margin: 0px; padding: 0px"></select>
									</div>
									
									<div style="display: inline;">
									
										<div id="asdasd" style="display: inline;background: #DDDDDD; padding-top:9px ; padding-bottom: 8px; margin-left: 5px;"><button id="search" onclick="getStockStatus()" style="cursor: pointer; height: 29px; margin: 0px; padding: 0px"><i class="fa fa-search" aria-hidden="true"></i></button></div>
										<div id="print" onclick="pop_print()" style="cursor:pointer; display: inline;background: #DDDDDD; padding-top:9px ; padding-bottom: 8px; margin-left: 5px;"><button  style="cursor: pointer; height: 29px; "><i class="fa fa-print" aria-hidden="true"></i> <spring:message code="print" ></spring:message></button></div></div>
									</div>
								</div>
							</td>
						</Tr>		
						<tr>
							<td>
								<div id="grid">
								</div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	<div id="print_table" style="display: none;"></div>
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	