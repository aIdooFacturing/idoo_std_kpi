<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Dash Board</title>
<style type="text/css">
body{
	background-color : black;
}
</style>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<script type="text/javascript">
	var login_lv = window.sessionStorage.getItem("lv");

	$(function(){
		setEl();
		timer();
		bindEvt();
		chkBanner();
	});
	
	function bindEvt(){
		$("img").not("#grid, #aIdoo, .flag, #logo, #logo2, #enter").hover(function(){
			var id = $(this).attr("name");
			$(this).attr("src", "${ctxPath}/images/icons/" + id + "_select.png");
		}, function(){
			var id = $(this).attr("name");
			$(this).attr("src", "${ctxPath}/images/icons/" + id + ".png");
		})
	};
	
	var handle = 0;
	function timer(){
		$("#today").html(getToday());
		handle = requestAnimationFrame(timer);
	};
	
	function setEl(){
		$("body").css({
			"margin" : 0,
			"padding" : 0,
			/* "background" : "url(" + ctxPath + "/images/home_bg.png)",
			"background-size" : "100%", */
			"background-color" : "black",
			"overflow" : "hidden"
		});
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
			"background-color" : "black ",
			"text-align" : "center"
		});
		
		$("#container").css({
			"margin-top" : (originHeight/2) - ($("#container").height()/2), 
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
		});
		
		$("#today").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#logo3").css({
			"margin-left" : getElSize(30)	
		});
		
		$("#iconTable img").css({
			"cursor" : "pointer",
			"height" : getElSize(450),
		}).click(showMenu);
		
		$("#panel").css({
			"background-color" : "rgb(34,34,34)",
			"border" : getElSize(20) + "px solid rgb(50,50,50)",
			"position" : "absolute",
			"width" : contentWidth * 0.2,
			"top" : 0,
			"left" : -contentWidth * 0.2 - getElSize(40),
			"z-index" : 99999
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.3,
			"position" : "absolute",
			"background-color" : "white",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		});
		
		$("#iconTable").css({
			//"margin-top" : getElSize(300) + marginHeight	
		});
		
		$("#iconTable td span").css({
			"color" : "white",
			"font-size" : getElSize(90)
		});
		
		$("#iconTable td").css({
			"text-align" : "center",
			"padding-left" : getElSize(240),
			"padding-right" : getElSize(240),
			"padding-top" : getElSize(120),
			"padding-bottom" : getElSize(150),
		});
		
		/* $("#corver").css({
			"width" : originWidth,
			"height" : originHeight,
			"position" : "absolute",
			"z-index" : 9,
			//"background-color" : "rgba(0,0,0,0.5)"
		}); */
		
		$("#iconTable img, span").css({
			"-webkit-filter" : "blur(" + getElSize(20) + "px)"
		});
		
		
		//loginform
		$("#enter").css({
			"width" : getElSize(200)
		});
		
		$("#loginForm").css({
			"width" : getElSize(1000),
			"height" : getElSize(900),
			"z-index" : 100,
			"background-color" : "white", 
			"position" : "absolute",
			"border-radius" : getElSize(30) + "px"
		});
		
		$("#loginForm").css({
			"left" : (window.innerWidth/2) - ($("#loginForm").width()/2),
			"top" : (window.innerHeight/2) - ($("#loginForm").height()/2)
		});
		
		$("#login #email").css({
			"height" :  getElSize(150) + "px",
			"width" : getElSize(700) + "px", 
			"font-size" : getElSize(50) + "px", 
			"padding" :  getElSize(30) + "px" , 
			"border-top-left-radius" : getElSize(30) + "px", 
			"border-top-right-radius" : getElSize(30) + "px", 
			"outline" : "0", 
			"border" :  getElSize(3) + "px solid rgb(235,234,219)"
		}).focus();

		$("#login #pwd").css({
			"height" :  getElSize(150) + "px",
			"width" : getElSize(700) + "px", 
			"font-size" : getElSize(50) + "px", 
			"padding" :  getElSize(30) + "px" , 
			"border-bottom-left-radius" : getElSize(30) + "px", 
			"border-bottom-right-radius" : getElSize(30) + "px", 
			"outline" : "0", 
			"border" :  getElSize(5) + "px solid rgb(235,234,219)"
		}); 

		$("#logo").css({ 
			"width" : getElSize(500) + "px",
			"margin-top" : getElSize(50) + "px"
		});

		$("#logo2").css({
			"width" : getElSize(300) + "px", 
			"margin-bottom" : getElSize(50) + "px",
			"-ms-interpolation-mode" : "bicubic"
		});

		$("#hr").css({
			"margin-top" : getElSize(50) + "px",
			"width" : "80%"	,
			"border" : "1px solid black"
		});

		$("#saveIdText").css({
			"margin-top" : getElSize(20),
			"font-size" : getElSize(40)	
		});
		
		$(".errMsg").css({
			"color" : "red", 
			"font-size" : getElSize(40) + "px"	
		});
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(80) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#grid").css({
			"position" : "absolute",
			"width" : contentWidth * 0.8,
		});
		
		$("#grid").css({
			"left" : (window.innerWidth/2) - ($("#grid").width()/2),
			"top" : getElSize(700) + marginHeight
		});
		
		$("#iconTable tr:nth(0) td img").not("#grid").css({
			"margin-top" : getElSize(600)
		});
		
		$("#aIdoo").css({
			"position" : "absolute",
			"width" : getElSize(650)
		});
		
		$("#aIdoo").css({
			"left" : (window.innerWidth/2) - ($("#aIdoo").width()/2),
			"top" : marginHeight + getElSize(150)
		});
		
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function goPage(el){
		var url = $(el).attr("url");
		if(url=="url"){
			alert("준비 중입니다.");
			return;
		}
		
		if($(el).attr("disable")=="true"){
			alert("권한이 없습니다.");
			return;
		};
		
		if(url == "banner"){
			getBanner();
			closePanel();
			$("#bannerDiv").css({
				"z-index" : 9999,
				"opacity" : 1
			});
			panel_flag = false;
			return;
		};
		
		location.href = "${ctxPath}/chart/" + url; 
	};
	 
	function showMenu(){
		var id = this.id;
		
		console.log(id)
		if(id=="monitor"){
			location.href = "${ctxPath}/chart/main.do";
		}else if(id=="analysis"){
			location.href = "${ctxPath}/chart/performanceAgainstGoal_chart_kpi.do";
		}else if(id=="inven"){
			location.href = "${ctxPath}/chart/delivery.do";
		}else if(id=="maintenance"){
			location.href = "${ctxPath}/chart/traceManager.do";
		}else if(id=="order"){
			location.href = "${ctxPath}/order/addTarget.do";
		}else if(id=="quality"){
			location.href = "${ctxPath}/chart/checkPrdct.do";
		}else if(id=="tool"){
			location.href = "${ctxPath}/chart/toolLifeManager.do";
		}else if(id=="kpi"){
			location.href = "${ctxPath}/kpi/productionStatusKpi_backUp.do";
		}
	};
	
	
	function showPanel(){
		$("#panel").animate({
			"left" : 0
		});
	};

	function closePanel(){
		$("#panel").animate({
			"left" : -contentWidth * 0.2 - getElSize(40)
		});
	};

	
	var monitor_menu = [
	                  ["${layout}", "main.do",  1],
	                  ["${dailydevicestatus}", "rotateChart.do",  1],
	                  ["${devicestatus}", "singleChartStatus.do",  1],
	                  ["${prdct_board}", "prdStatus.do",  1],
	                  ["${barchart}", "main3.do",  1]
	                  ];
	
	var analysis_menu = [
	                  ["${performance_chart}", "performanceAgainstGoal.do",  1],
	                  ["${operation_chart}", "performanceReport.do",  1],
	                  ["${prdct_chart}", "performanceAgainstGoal_chart.do",  1],
	                  ["${operation_graph}", "jigGraph.do",  1],
	                  ["${operation_graph_daily}", "wcGraph.do",  1],
	                  ["${check_program_error}", "programManager.do",  1]
	                  ];
	
	var kpi_menu = [
	                  ["${prdct_chart}", "performanceAgainstGoal_chart_kpi.do",  1],
	                  ["${operation_graph}", "jigGraph_kpi.do",  1],
	                  ["${lead_time}", "leadTime.do",  1],
	                  ["${faulty_ratio_customr_and_operation}", "faulty.do",  1]
	                  ];
	
	var quality_menu = [
	                  ["${f_m_l_check}", "checkPrdct.do",  1],
	                  ["초.중.종물 검사기준 관리", "checkPrdctStandard.do",  1],
	                  ["불량 등록", "addFaulty.do",  1],
	                  ["불량 등록 조회", "addFaultyHistory.do",  1],
	                  ["고객 불량율 / 공정 불량율", "faulty.do",  1],
	                  ["로트 추적 조회", "lotTracer.do",  1]
	                  ];
	
	var tool_menu = [
	                  ["툴 수명 주기 관리", "toolManager.do",  1],
	                  ["프로그램 별 가공 이상 분석", "programManager.do",  1],
	                  ["재가공 의심 생산 사이클", "reOpCycle.do",  1]
	                  ]; 
	
	var order_menu = [
	                  ["일 생산 계획 등록", "addTarget.do",  1],
	                  ["생산 완료 입력", "addPrdCmpl.do",  1],
	                  ["생산 완료 입력 조회", "addPrdCmplHistory.do",  1],
	                  ["비가동 입력", "addNoOperation.do",  1],
	                  ["비가동 내역 조회", "addNoOperationHistory.do",  1]
	                  ];
	
	var inven_menu = [
	                  ["${income_manage}", "incomeStock.do",  1],
	                  ["${income_history}", "incomeStockHistory.do",  1],
	                  ["${release_manage}", "releaseStock.do",  1],
	                  ["${release_history}", "releaseStockHistory.do",  1],
	                  ["${ship_manage}", "shipment.do",  1],
	                  ["${ship_history}", "shipmentHistory.do",  1],
	                  ["${move_operation}", "transferOpr.do",  1],
	                  ["${stock_status}", "stockStatus.do",  1],
	                  ];
	
	var maintenance_menu = [
	                  ["Alarm발생 이력 조회", "alarmReport.do",  1],
	                  ["보유 장비 현황", "traceManager.do",  1],
	                  ["Catch Phrase관리", "banner",  1],
	                  ["원격 TV Control", "url",  1],
	                  ["직원 관리", "workerMaanger.do",  5],
	                  ];
	

	var shopId = 1;
	
	function chkKeyCd(evt){
		if(evt.keyCode==13) login();
	};
	
</script>
</head>
<body>	
	
	<!-- <div id="corver"></div> -->
	<div id="intro_back"></div>
	<span id="intro"></span>

	<%-- <jsp:include page="headerJsp.jsp"></jsp:include> --%>
	<div id="today"></div>
	<div id="title_right"></div>
	<img alt="" src="${ctxPath }/images/aIdoo.png" id="aIdoo">
	<img alt="" src="${ctxPath }/images/grid.png" id="grid">
	
	<div id="container">
		<div id="panel">
			<table id="panel_table" width="100%">
			</table>
		</div>
		<Center>
			<table id="iconTable" style="width: 100%">
				<Tr>
					<td style="width: 25%">
						<img alt="" src="${ctxPath }/images/icons/Monitoring.png" id="monitor" name="Monitoring"><Br>		
					</td>
					<td style="width: 25%">
						<img alt="" src="${ctxPath }/images/icons/Analysis.png" id="analysis" name="Analysis"><Br>
					</td>
					<Td style="width: 25%">
						<img alt="" src="${ctxPath }/images/icons/Tool Management.png" id="tool" name="Tool Management"><Br>
											
					</Td>
					<td style="width: 25%">
						<img alt="" src="${ctxPath }/images/icons/Maintenance.png" id="maintenance" name="Maintenance"><Br>
						
					</td>
				</Tr>
				<tr>
					<td>
						<img alt="" src="${ctxPath }/images/icons/Master Data.png" id="inven" name="Master Data"><Br>
						
					</td>
					<td>
						<img alt="" src="${ctxPath }/images/icons/Order Management.png" id="order" name="Order Management"><Br>
					</td>
					<Td>
						<img alt="" src="${ctxPath }/images/icons/Quality Management.png" id="quality" name="Quality Management"><Br>
					</Td>
					<td>
						<img alt="" src="${ctxPath }/images/icons/Advanced Report.png" id="kpi" name="Advanced Report"><Br>												
					</td>
				</tr>
			</table>	
		</Center>
	</div>
</body>
</html>	