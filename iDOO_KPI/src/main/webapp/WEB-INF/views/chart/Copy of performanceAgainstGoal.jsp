<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="prdct_machine_line" var="prdct_machine_line"></spring:message>
<spring:message code="line" var="line"></spring:message>
<spring:message code="device" var="device"></spring:message>
<spring:message code="standard" var="standard"></spring:message>
<spring:message code="worker" var="worker"></spring:message>
<spring:message code="prdct_performance" var="prdct_performance"></spring:message>
<spring:message code="op_performance" var="op_performance"></spring:message>
<spring:message code="name" var="name"></spring:message>
<spring:message code="n_d" var="n_d"></spring:message>
<spring:message code="work" var="work"></spring:message>
<spring:message code="assiduity" var="assiduity"></spring:message>
<spring:message code="machine_performance" var="machine_performance"></spring:message>
<spring:message code="performance_insert" var="performance_insert"></spring:message>
<spring:message code="faulty" var="faulty"></spring:message>
<spring:message code="plan" var="plan"></spring:message>
<spring:message code="target_ratio" var="target_ratio"></spring:message>
<spring:message code="faulty_ratio" var="faulty_ratio"></spring:message>
<spring:message code="target" var="target"></spring:message>
<spring:message code="incycle" var="incycle"></spring:message>
<spring:message code="wait" var="wait"></spring:message>
<spring:message code="stop" var="stop"></spring:message>
<spring:message code="avg_cycle_time" var="avg_cycle_time"></spring:message>
<spring:message code="op_ratio" var="op_ratio"></spring:message>
<spring:message code="sum" var="sum"></spring:message>
<spring:message code="total" var="total"></spring:message>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<title><spring:message code="performance_chart"></spring:message></title>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}
	
</style>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;
	var wcData;
	var jigData;
	
	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	function csvSend(){
		var id = this.id;
		var sDate, eDate;
		var csvOutput;
		
		if(id=="jigExcel"){
			sDate = $("#jig_sdate").val();
			csvOutput = jigData;
		}else{
			sDate = $("#wc_sdate").val();
			csvOutput = wcData;
		};
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sDate;
		f.endDate.value = "";
		f.submit(); 
	};
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<script src="${ctxPath}/js/jquery.table2excel.js"></script>
<script src="${ctxPath}/js/jquery.base64.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<script type="text/javascript">

var shopId = 1;

function exportExcel(){
	var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('tableContainer');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');

    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    a.download = 'exported_table_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
    a.click();
  
};

var save_type = "init"

	function getBanner(){
	var url = "${ctxPath}/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100);
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = true;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 100)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},15000, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};

function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		$("#intro_back").css("display","block");
		getBanner();
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};

function cancelBanner(){
	window.localStorage.setItem("banner", "false");
	$("#bannerDiv").css("z-index",-9);
	chkBanner();
};

function addBanner(){
	var url =  ctxPath + "/chart/addBanner.do";
	var param = "msg=" + $("#banner").val() + 
				"&rgb=" + $("#color").val() + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			window.localStorage.setItem("banner", "true");
			$("#bannerDiv").css("z-index",-9);
			//chkBanner();
			location.reload();
		}
	});
};

function getGroup(){
	var url = "${ctxPath}/chart/getGroup.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			json = data.dataList;
			
			var option = "<option value='all'>전체</option>";
			
			$(json).each(function(idx, data){
				option += "<option value='" + data.group + "'>" + data.group + "</option>"; 
			});
			
			$("#group").html(option);
			
			getTableData("jig");
		}
	});
};

$(function(){
	getGroup();
	
	setDate();			
	$(".excel").click(csvSend);
	setEl();
	chkBanner();
	setEvt();
	$("#menu_btn").click(function(){
			location.href = "${ctxPath}/chart/index.do"
		});
	
	$(".menu").click(goReport);
	
	$("#panel_table td").addClass("unSelected_menu");
	$("#menu12").removeClass("unSelected_menu");
	$("#menu12").addClass("selected_menu");
	
	
	$("#dlgTable").css({
		"width" : "95%",
		"display" : "none",
		"background-color" : "#505050",
		"border-radius" : getElSize(30)
	});
	
	$("#dlgTable").css({
		"position" : "absolute",
		"z-index" : -1,
		"left" : originWidth/2 - ($("#dlgTable").width()/2),
		"top" : originHeight/2 - ($("#dlgTable").height()/2)
	});
	
	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0,
		"top" : 0
	});
	
	$("#group").css({
		"font-size" : getElSize(60)
	})

});

var jigCsv;
var wcCsv;
var selected_dvc;
var className = "";
var classFlag = true;
var rowSpanArray = [];
var rowSpanArrayLine = [];

function getMachineTy(name){
	ty = "";
	if(decode(name).indexOf("내수")!=-1){
		ty = "내수 ";
	}else if(decode(name).indexOf("북미")!=-1){
		ty = "북미 ";
	}else if(decode(name).indexOf("유럽")!=-1){
		ty = "유럽 ";
	};
	
	name = ty + decode(name).replace(/\+/gi, " ").substr(decode(name).lastIndexOf("#")-1,1);
	return name;
};

function getTableData(el){
	classFlag = true;
	var sDate = $("#jig_sdate").val();
	var eDate = $("#jig_edate").val() + " 23:59:59";
	
	var url = "${ctxPath}/chart/getPerformanceAgainstGoal.do";

	window.localStorage.setItem("jig_sDate", sDate);
	var param = "sDate=" + sDate + 
				"&eDate=" + eDate +
				"&shopId=" + shopId + 
				"&group=" + $("#group").val();
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			rowSpanArray = [];
			rowSpanArrayLine = [];
			var preName = json[0].group;
			var preLine = getMachineTy(json[0].name)
			var i = 0;
			var j = 0;
			
			var array = [];
			
			var sum_capa = 0;
			var sum_target = 0;
			var sum_cnt = 0;
			var sum_dvc_cnt = 0;
			var sum_fault = 0;
			var sum_targetRatio = 0;
			var sum_faultRatio = 0;
			var sum_target_time = 0;
			var sum_incycle_time = 0;
			var sum_wait_time = 0;
			var sum_alarm_time = 0;
			var sum_noConn_time = 0;
			var sum_AvrCycleTime = 0;
			var sum_opRatio = 0;
			
			var total_capa = 0;
			var total_target = 0;
			var total_cnt = 0;
			var total_dvc_cnt = 0;
			var total_fault = 0;
			var total_targetRatio = 0;
			var total_faultRatio = 0;
			var total_target_time = 0;
			var total_incycle_time = 0;
			var total_wait_time = 0;
			var total_alarm_time = 0;
			var total_noConn_time = 0;
			var total_AvrCycleTime = 0;
			var total_opRatio = 0;
			
			$(json).each(function(idx, data){
				if(preName!=data.group){
					preName = data.group;
					rowSpanArray.push(i+1);
					i = 0;
					
					array = [j, preLine]
					rowSpanArrayLine.push(array);
					j = 0;
					preLine = "re";
				};
				i++;
				
				if(preLine!=getMachineTy(data.name)){
					if(j!=0){
						array = [j,preLine]
						rowSpanArrayLine.push(array);
						j = 0;	
					}
					preLine = getMachineTy(data.name);
				};
				j++;
			});
			
			sum = 0;
			for(var i = 0; i < rowSpanArray.length; i++){
				sum += rowSpanArray[i]-1;
			};
			
			rowSpanArray.push(json.length - sum+1);
			
			sum = 0;
			for(var i = 0; i < rowSpanArrayLine.length; i++){
				sum += rowSpanArrayLine[i][0];
			};
			
			array = [json.length - sum, getMachineTy(json[json.length-1].name)]
			rowSpanArrayLine.push(array);
			
			//$(".contentTr").remove();
			//var tr = "<tbody>";
			var target_ratio = "${target_ratio}";
			var tr = "<thead>" + 
					"<tr style='font-weight: bolder; background-color: rgb(34,34,34)' class='thead'>" + 
						"<Td rowspan='2'  >${prdct_machine_line}</Td>" + 
						"<Td rowspan='2'  >${line}</Td>" + 
						"<Td rowspan='2' >${device}</Td>" + 
						"<td colspan='4' >${standard}</td>" + 
						"<td colspan='4'>${worker}</td>" + 
						"<td colspan='7'>${prdct_performance}</td>" + 
						"<td colspan='7'>${op_performance}</td>" + 
					"</tr>" + 
					"<tr style='font-weight: bolder; background-color: rgb(34,34,34)' class='thead'>" + 
						"<td  >Cycle<Br>Time(분)</td>" + 
						"<td  >Cycle<br>Time(초)</td>" +  
						"<td  >Cavity</td>" + 
						"<td  >UPH</td>" + 
						"<td  >${name}</td>" + 
						"<td  >${n_d}</td>" + 
						"<td  >${work}</td>" + 
						"<td  >${assiduity}</td>" + 
						"<td >Capa</td>" + 
						"<td >${plan}</td> " +
						"<td >${machine_performance}</td>" + 
						"<td >${performance_insert}</td>" + 
						"<td >${faulty}</td>" + 
						"<td >" + target_ratio.replace(/&nbsp;/gi,"") + "</td>" +  
						"<td >${faulty_ratio}</td>" + 
						"<td >${target}</td>" + 
						"<td >${incycle}</td>" + 
						"<td >${wait}</td>" + 
						"<td >${stop}</td>" + 
						"<td >Power Off</td>" + 
						"<td >${avg_cycle_time}</td>" + 
						"<td >${op_ratio}</td>" + 
					"</tr>" + 
				"</thead>";
				
		//	jigData = "생산차종,장비,근무시간,생산 Capa (EA), 생산 계획 (EA),생산 실적(EA),달성율 (%)LINE";
			
			var preGroup = "";
			var preLine = "";
			i = 0;
			j = 0;
			var rowSpan = 1;
			var rowSpanLine = 1;
			
			var dvcCnt = 0;
			var totaldvcCnt = 0;
			var first = true;
			$(json).each(function(idx, data){
				if(classFlag){
					className = "row2"
				}else{
					className = "row1"
				};
				classFlag = !classFlag;
				
				var group = "";
				var total = "";
				
				if(preGroup!=data.group){
					rowSpan = rowSpanArray[i];

					preGroup = data.group;
					group = "<td align='center' rowspan=" +(rowSpan*2-1) + " ;>" + data.group + "</td>";
					
					i++;

					rowSpanLine = rowSpanArrayLine[j][0];
					preLine = "re";
					line = "<td align='center' rowspan=" + (rowSpanLine*2) + " ;>" + rowSpanArrayLine[j][1] + "</td>";
					
				}else{
					group = "";
				};
				
				var line = "";
				if(preLine!=getMachineTy(data.name)){
					rowSpanLine = rowSpanArrayLine[j][0];
					
					preLine = getMachineTy(data.name);
					
					var lineName = rowSpanArrayLine[j][1];
					if(lineName.indexOf("M")!=-1){
						lineName = rowSpanArrayLine[j][1] + "CT " + rowSpanArrayLine[j][0] + "대";
					}else if(lineName.indexOf("C")!=-1){
						lineName = rowSpanArrayLine[j][1] + "NC " + rowSpanArrayLine[j][0] + "대";
					}else if(lineName.indexOf("R")!=-1){
						lineName = rowSpanArrayLine[j][1] + "삭 " + rowSpanArrayLine[j][0] + "대";
					}
					
					line = "<td align='center' rowspan=" + (rowSpanLine*2) + " >" + lineName + "</td>";
					j++;
					
				}else{
					line = "";
				};
				
				
				if(!first && group!=""){
					tr += "<tr>" +
							"<Td colspan='10'>${sum}</td>" +
							"<Td>" + sum_capa + "</td>" +
							"<Td>" + sum_target + "</td>" +
							"<Td>" + sum_dvc_cnt + "</td>" +
							"<Td>" + sum_cnt + "</td>" +
							"<Td>" + sum_fault + "</td>" +
							"<Td>" + Number(sum_targetRatio/dvcCnt).toFixed(1) + "%</td>" +
							"<Td>" + Number(sum_faultRatio/dvcCnt).toFixed(1) + "%</td>" +
							"<Td>" + Number(sum_target_time).toFixed(1) + "</td>" +
							"<Td>" + Number(sum_incycle_time).toFixed(1) + "</td>" +
							"<Td>" + Number(sum_wait_time).toFixed(1) + "</td>" +
							"<Td>" + Number(sum_alarm_time).toFixed(1) + "</td>" +
							"<Td>" + Number(sum_noConn_time).toFixed(1) + " </td>" +
							"<Td>" + Number(sum_AvrCycleTime).toFixed(1) + "</td>" +
							"<Td>" + Number(sum_opRatio/dvcCnt).toFixed(1) + "%</td>" +
						"</tr>";
						
					sum_capa = 0;
					sum_target = 0;
					sum_cnt = 0;
					sum_dvc_cnt = 0;
					sum_fault = 0;
					sum_targetRatio = 0;
					sum_faultRatio = 0;
					sum_target_time = 0;
					sum_incycle_time = 0;
					sum_wait_time = 0;
					sum_alarm_time = 0;
					sum_noConn_time = 0;
					sum_AvrCycleTime = 0;
					sum_opRatio = 0;
					
					dvcCnt = 0;
				};
				
				var faultRatio = Number(data.faultCnt / data.cntCyl * 100).toFixed(1);
				if(data.cntCyl==0) faultRatio = 0;
			
				sum_capa += Number(data.capa);
				sum_target += Number(data.tgCyl);
				sum_cnt += Number(data.cntCyl);
				sum_dvc_cnt += Number(data.dvcCntCyl);
				sum_fault += Number(data.faultCnt);
				sum_targetRatio += Number(data.targetRatio);
				sum_faultRatio += Number(faultRatio);
				sum_target_time += Number(data.target_time/3600);
				sum_incycle_time += Number(data.inCycle_time/3600);
				sum_wait_time += Number(data.wait_time/3600);
				sum_alarm_time += Number(data.alarm_time/3600);
				sum_noConn_time += Number(data.noConnTime/3600);
				sum_AvrCycleTime += Number(data.lastAvrCycleTime/60);
				sum_opRatio += Number(data.opRatio);
				
				total_capa += Number(data.capa);
				total_target += Number(data.tgCyl);
				total_cnt += Number(data.cntCyl);
				total_dvc_cnt += Number(data.dvcCntCyl);
				total_fault += Number(data.faultCnt);
				total_targetRatio += Number(data.targetRatio);
				total_faultRatio += Number(faultRatio);
				total_target_time += Number(data.target_time/3600);
				total_incycle_time += Number(data.inCycle_time/3600);
				total_wait_time += Number(data.wait_time/3600);
				total_alarm_time += Number(data.alarm_time/3600);
				total_noConn_time += Number(data.noConnTime/3600);
				total_AvrCycleTime += Number(data.lastAvrCycleTime/60);
				total_opRatio += Number(data.opRatio);
				
				dvcCnt++;
				totaldvcCnt++;
				tr += "<tr class='contentTr " + className + "' >" +
							group + 
							line + 
							"<td rowspan='2' >" + decode(data.name).replace(/\+/gi, " ").substr(decode(data.name).lastIndexOf("#")-1) + "</td>" +
							"<Td rowspan='2' '>" + data.cycleTmM + "</td>" +
							"<Td rowspan='2' >" + data.cycleTmS + "</td>" +
							"<Td rowspan='2' >" + data.cvt + "</td>" + 
							"<Td rowspan='2' >" + data.uph +"</td>" + 
							"<Td >" + decodeURIComponent(data.workerD).replace(/\+/gi, " ") + "</td>" + 
							"<Td >주간</td>" + 
							"<Td >" + data.workTmMD + "</td>" + 
							"<Td >" + data.nonOpTimeD + "</td>" + 
							"<Td rowspan='2'>" + data.capa + "</td>" + 
							"<Td rowspan='2'>" + data.tgCyl + "</td>" +
							"<Td rowspan='2'>" + data.dvcCntCyl + "</td>" + 
							"<Td rowspan='2'>" + data.cntCyl + "</td>" + 
							"<Td rowspan='2'>" + data.faultCnt + "</td>" + 
							"<Td rowspan='2'>" + data.targetRatio + "%</td>" + 
							"<Td rowspan='2'>" + faultRatio + "%</td>" + 
							"<Td rowspan='2'>" + Number(data.target_time/3600).toFixed(1) + "</td>" + 
							"<Td rowspan='2' >" + Number(data.inCycle_time/3600).toFixed(1) + "</td>" + 
							"<Td rowspan='2' >" + Number(data.wait_time/3600).toFixed(1) + "</td>" + 
							"<Td rowspan='2' >" + Number(data.alarm_time/3600).toFixed(1) + "</td>" + 
							"<Td rowspan='2' >" + Number(data.noConnTime/3600).toFixed(1) + "</td>" +  
							"<Td rowspan='2' >" + Number(data.lastAvrCycleTime/60).toFixed(1) + "</td>" +
							"<Td rowspan='2' >" + Number(data.opRatio).toFixed(1) + "%</td>" + 
					 "</tr>" + 
					 "<tr class='contentTr " + className + "' >" + 
					 	"<td  '>" + decodeURIComponent(data.workerN).replace(/\+/gi, " ") + "</td>" +
					 	"<td  '>야간</td>" +
					 	"<td  '>" + data.workTmMN + "</td>" +
					 	"<td '>" + data.nonOpTimeN + "</td>" + 
					 "</tr>";
					 
			
			first = false;

			});
			
			tr += "<tr>" +
					"<Td colspan='10'>소계</td>" +
					"<Td>" + sum_capa + "</td>" +
					"<Td>" + sum_target + "</td>" +
					"<Td>" + sum_dvc_cnt + "</td>" +
					"<Td>" + sum_cnt + "</td>" +
					"<Td>" + sum_fault + "</td>" +
					"<Td>" + Number(sum_targetRatio/dvcCnt).toFixed(1) + "%</td>" +
					"<Td>" + Number(sum_faultRatio/dvcCnt).toFixed(1) + "%</td>" +
					"<Td>" + Number(sum_target_time).toFixed(1) + "</td>" +
					"<Td>" + Number(sum_incycle_time).toFixed(1) + "</td>" +
					"<Td>" + Number(sum_wait_time).toFixed(1) + "</td>" +
					"<Td>" + Number(sum_alarm_time).toFixed(1) + "</td>" +
					"<Td>" + Number(sum_noConn_time).toFixed(1) + " </td>" +
					"<Td>" + Number(sum_AvrCycleTime).toFixed(1) + "</td>" +
					"<Td>" + Number(sum_opRatio/dvcCnt).toFixed(1) + "%</td>" +
				"</tr>" +  
				"<tr>" +
					"<Td colspan='11'>${total}</td>" +
					"<Td>" + total_capa + "</td>" +
					"<Td>" + total_target + "</td>" +
					"<Td>" + total_dvc_cnt + "</td>" +
					"<Td>" + total_cnt + "</td>" +
					"<Td>" + total_fault + "</td>" +
					"<Td>" + Number(total_targetRatio/totaldvcCnt).toFixed(1) + "%</td>" +
					"<Td>" + Number(total_faultRatio/totaldvcCnt).toFixed(1) + "%</td>" +
					"<Td>" + Number(total_target_time).toFixed(1) + "</td>" +
					"<Td>" + Number(total_incycle_time).toFixed(1) + "</td>" +
					"<Td>" + Number(total_wait_time).toFixed(1) + "</td>" +
					"<Td>" + Number(total_alarm_time).toFixed(1) + "</td>" +
					"<Td>" + Number(total_noConn_time).toFixed(1) + "</td>" +
					"<Td>" + Number(total_AvrCycleTime).toFixed(1) + "</td>" +
					"<Td>" + Number(total_opRatio/totaldvcCnt).toFixed(1) + "%</td>" +
				"</tr>" +
				"</tbody>"; 
			
				
			$(".tmpTable").html(tr);
		 	$(".tmpTable td").css({
				"border" : getElSize(2) + "px solid black",
				"font-size" : getElSize(40)	
			});
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#222222",
				"font-size" : getElSize(40)
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#323232",
				"font-size" : getElSize(40)
			}); 
			
			//setEl();
			
			$(".tableContainer div:last").remove()
			scrolify($('.tmpTable'), getElSize(1550));
			
			$("*").not(".container, #dvcDiv").css({
				"overflow-x" : "hidden",
				"overflow-y" : "auto"
			})
		}
	});
};

var dvc;
function replaceHyphen(str){
	return str.replace(/#/gi,"-");	
};


function goReport(){
	var type = this.id;
	var url;
	if(type=="menu0"){
		url = "${ctxPath}/chart/main.do";
		location.href = url;
	}else if(type=="menu1"){
		url = "${ctxPath}/chart/performanceReport.do";
		location.href = url;
	}else if(type=="menu2"){
		url = "${ctxPath}/chart/alarmReport.do";
		location.href = url;
	}else if(type=="menu3"){
		url = "${ctxPath}/chart/main3.do";
		location.href = url;
	}else if(type=="menu9"){
		url = "${ctxPath}/chart/wcGraph.do";
		window.localStorage.setItem("dvcId", 1)
		location.href = url;
	}else if(type=="menu4"){
		url = ctxPath + "/chart/rotateChart.do";
		location.href = url;
	}else if(type=="menu5"){
		url = "${ctxPath}/chart/traceManager.do";
		location.href = url;
	}else if(type=="menu6"){
		url = "${ctxPath}/chart/toolManager.do";
		location.href = url;
	}else if(type=="menu8"){
		url = "${ctxPath}/chart/jigGraph.do";
		location.href = url;
	}else if(type=="menu9"){
		url = "${ctxPath}/chart/wcGraph.do";
		location.href = url;
	}else if(type=="menu10"){
		url = "${ctxPath}/chart/addTarget.do";
		location.href = url;
	}else if(type=="menu7"){
		url = "${ctxPath}/chart/singleChartStatus.do";
		location.href = url;
	}else if(type=="menu11"){
		url = "${ctxPath}/chart/prdStatus.do";
		location.href = url;
	}else if(type=="menu99"){
		$("#bannerDiv").css({
			"z-index" : 9999
		});
		getBanner();
		closePanel();
		panel = false;
	}else if(type=="menu12"){
		url = "${ctxPath}/chart/performanceAgainstGoal.do";
		location.href = url;
	}else if(type=="menu100"){
		url = "${ctxPath}/chart/stockStatus.do";
		location.href = url;
	}else if(type=="menu101"){
		url = "${ctxPath}/chart/programManager.do";
		location.href = url;
	}else if(type=="menu102"){
		url = "${ctxPath}/chart/leadTime.do";
		location.href = url;
	}else if(type=="menu103"){
		url = "${ctxPath}/chart/faulty.do";
		location.href = url;
	}else if(type=="menu104"){
		url = "${ctxPath}/chart/addFaulty.do";
		location.href = url;
	}
};

var panel = false;
function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};

function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function setDate(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	$(".date").val(year + "-" + month + "-" + day);
};

function setEvt(){
	
	
};

function showJigList(){
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
	
	$("#jigList").toggle();	
};

function dvcList(){
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
	
	$("#dvcList").toggle();	
};


function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
		
	});
	
	$("#dlgTable").css({
		"z-index":-1,
		"display" : "none"
	});
	$("#dvcList").css("display","none");
};

function setEl() {
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight,
		"overflow" : "hidden"
		//"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing" : getElSize(30)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	
	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(130),
		"left" : 0,
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});

	$("#panel").css({
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"top" : 0,
		"left" : -contentWidth * 0.2 - getElSize(40),
		"z-index" : 99999
	});
	
	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(40)
	});

	$("#panel_table td").css({
		"padding" : getElSize(30),
		"cursor" : "pointer"
	});

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	
	$(".tableContainer").css({
		"height" : getElSize(1700),
		"overflow" : "hidden",
		"width" : "100%"
	});
	
	$(".tableContainer").css({
		"margin-left" : (contentWidth/2) -($(".tableContainer").width()/2)		
	});
	
	$("#dvcDiv").css({
		"margin-left" : $(".tableContainer").offset().left - marginWidth,
		"width" : $(".tableContainer").width(),
		"margin-bottom" : getElSize(50),
		"color" : "white"
	})
	
	$(".goGraph, .excel, .label, #wcSelector").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	/* $(".thead").css({
		"font-size" : getElSize(40)
	});
	
	$(".thead td").css({
		"border" : getElSize(5) + "px solid white"
	}); */
	
	/* $(".contentTr, .contentTr2").css({
		"font-size" : getElSize(40)
	});
	 */
	 
	$(".machineListForTarget").css({
		"position" : "absolute",
		"width" : getElSize(1200),
		"height" : getElSize(1200),
		"overflow" : "auto",
		//"top" : getElSize(50),
		//"background-color" : "rgb(34,34,34)",
		"background-color" : "green",
		"color" : "white",
		"font-size" : getElSize(50),
		"padding" : getElSize(50),
		"overflow" : "auto",
		"border-radius" : getElSize(50),
		"border" : getElSize(10) + "px solid white",
	});
	
	$(".machineListForTarget").css({
		"left" : (originWidth/2) - ($(".machineListForTarget").width()/2),
	});
	
	$("#bannerDiv").css({
		"position" : "absolute",
		"width" : getElSize(1500),
		"height" : getElSize(200),
		"border-radius" : getElSize(20),
		"padding" : getElSize(50),
		"background-color" : "lightgray",
		"z-index" : -9
	});

	
	$("#bannerDiv").css({
		"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
		"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
	});
	
	$("#bannerDiv input[type='text']").css({
		"width" : getElSize(1200),
		"font-size" : getElSize(50)
	});
	
	$("#bannerDiv button").css({
		"margin" : getElSize(50),
		"font-size" : getElSize(50)
	});
	
	$("#intro").css({
		/* "width" : contentWidth, */
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.3,
		"position" : "absolute",
		"background-color" : "white",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	});
	
};

</script>
</head>

<body >
	<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
	
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
		
	<div id="title_right" class="title"></div>	
	
	<img src="${ctxPath }/images/home.png" id="menu_btn" >
	
	
	<table id="dvcList" style="color:white; border-collapse: collapse;"></table>
			
	<div id="corver"></div>

	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
				<table class="mainTable">
					<tr>
						<Td align="center" style=" color:white; font-weight: bolder;" class="title" >
							<spring:message code="performance_chart"></spring:message>
						</Td>
					</tr>
				</table>
				
				<div id="dvcDiv">
					<select id="group"></select>
					<span class="label"><spring:message code="op_period"></spring:message> </span> 
					<input type="date" class="date" id="jig_sdate"> ~ <input type="date" class="date" id="jig_edate">
					<span style="background-color: white; color:black; font-weight: bolder; padding: 3px;" class="excel" id="jigExcel"><spring:message code="excel"></spring:message></span>
					<span style="background-color: white; color:black; font-weight: bolder; padding: 3px;" class="excel" onclick="getTableData()"><spring:message code="check"></spring:message></span>
				</div>

				<div class="tableContainer" style="width: 100%" id="tableContainer">
					<table style="color: white; width: 100%; text-align: center; border-collapse: collapse; " class="tmpTable"  id="table">
					</table>
				</div>
		</div>
	</div>
</body>
</html>