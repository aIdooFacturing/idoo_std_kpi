<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">

<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<script type="text/javascript">
$(function(){
	$(".excel").click(csvSend);
	setEl();
	setDate();
	
	$("#menu_btn").click(function(){
		if(!panel){
			showPanel();
		}else{
			closePanel();
		};
		panel = !panel;
	});
	
	$(".menu").click(goReport);
	$("#panel_table td").addClass("unSelected_menu");
	$("#menu3").removeClass("unSelected_menu");
	$("#menu3").addClass("selected_menu");
	
	//getMousePos();
	$(".date").change(getNightlyMachineStatus);
	
	getNightlyMachineStatus();
});

function getNightlyMachineStatus(){
	var url = "${ctxPath}/device/getNightlyMachineStatus.do";

	var param = "stDate=" + $("#sDate").val() + 
				"&edDate=" + $("#eDate").val()

	
	$.ajax({
		url : url,
		data : param,
		dataType : "text",
		type : "post",
		success : function(data){
			var json = $.parseJSON(data);
			
			var tr = "<Tr style='font-weight: bolder;background-color: rgb(34,34,34)' class='thead'>" + 
						"<td>No</td>" +
						"<td>설비명</td>" +
						"<td>날짜</td>" +
						"<td>정지시간</td>" +
						"<td>상태</td>" +
						"<td>기준 날짜</td>" +
					"</tr>";
					
			csvOutput = "No, 설비명, 날짜, 정지시간, 상태, 기준 날짜LINE";
			var status = "";
			var backgroundColor = "";
			var fontColor = "";
			
			$(json).each(function(idx, data){
				if(data.status=="WAIT" || data.status=="NO-CONNECTION"){
					status = "완료";
					fontColor = "black";
					backgroundColor = "yellow";
				}else if(data.status=='ALARM'){
					status = "중단";
					fontColor = "white";
					backgroundColor = "red";
				}else if(data.status=='IN-CYCLE'){
					status = "가동";
					fontColor = "white";
					backgroundColor = "green";
				};
				
				if(data.isOld=="OLD"){
					status = "미가동";
					fontColor = "white";
					backgroundColor = "black";
				};
				
				tr += "<tr style='font-Size:" + getElSize(50) + "px;'>"  +
					"<td  style='border:1px solid white;'>" + (idx+1)  + "</td>" +
					"<td  style='border:1px solid white;'> " + data.name + "</td>" +
					"<td style='border:1px solid white;'>" + data.stopDate + "</td>" +
					"<td style='border:1px solid white;'>" + data.stopTime + "</td>" + 
					"<td style='border:1px solid white; color:" + fontColor + "; background-color:" + backgroundColor + "'>" + status + "</td>" +
					"<td style='border:1px solid white;'>" + data.inputDate + "</td>" +
				"</tr>";
				
				csvOutput += (idx+1) + "," +
					data.name + "," + 
					data.stopDate + "," + 
					data.stopTime + "," + 
					status +  "," +
					data.inputDate + "LINE";
		
			});
			

			if(json.length==0){
				tr += "<tr  style='border:1px solid white; font-Size:" + getElSize(50) + "px;'>" + 
						"<td colspan='6'>없음</tb>" + 
					"<tr>";
			};
			
			$(".tmpTable").html(tr);
			setEl();			
		}
	});
};

var menu = false;
function getMousePos(){
	$(document).on("mousemove", function(e){
		var target = $("#menu_btn").width();

		if((e.pageX <= target && e.pageY <= target) || panel){
			if(!menu){
				$("#menu_btn").animate({
					"left" : 0,
					"top" : getElSize(20),
				});
				menu = true;
			};
		}else{
			if(menu){
				$("#menu_btn").animate({
					"left" : -getElSize(100),
				});	
				menu = false;
			};
		};
	});
};

function goReport(){
	var type = this.id;
	var url;
	if(type=="menu0"){
		url = "${ctxPath}/chart/main.do";
		location.href = url;
	}else if(type=="menu1"){
		url = "${ctxPath}/chart/performanceReport.do";
		location.href = url;
	}else if(type=="menu2"){
		url = "${ctxPath}/chart/alarmReport.do";
		location.href = url;
	}else if(type=="menu3"){
		url = "${ctxPath}/chart/DIMM.do";
		location.href = url;
	};
};

var panel = false;
function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};


function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function setDate(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	$(".date").val(year + "-" + month + "-" + day);
};

function showJigList(){
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
		
	});
	
	$("#jigList").toggle();	
};

function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
		
	});
	
	$("#jigList").css("display","none");
};

function setEl() {
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight,
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1
	});

	$(".title_right").css({
		"float" : "right",
		"width" : contentWidth * 0.1
	});

	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(100),
		"left" : 0,
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});
	

	$("#panel").css({
		"height" : originHeight,
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"width" : contentWidth * 0.2,
		"position" : "absolute",
		"color" : "white",
		"z-index" : 12,
		"left" : -contentWidth * 0.2 - (getElSize(20) * 2)
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(50)
	});

	$("#panel_table td").css({
		"padding" : getElSize(50),
		"cursor" : "pointer"
	});

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	$("#jigList").css({
		"position" : "absolute",
		"left" : originWidth/2 - $("#jigList").width()/2,
		"top" : originHeight/2 - $("#jigList").height()/2,
		"z-index" : 9999999,
		"display" : "none"
	});
	
	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});
	
	$(".goGraph, .excel, .label").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".tmpTable, .tmpTable tr, .tmpTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
	});
	
	$(".tmpTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100)
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(40)
	});
};

var csvOutput;
function csvSend(){
	var sDate, eDate;
	
	sDate = $("#sDate").val();
	eDate = $("#eDate").val();
	
	csvOutput = csvOutput.replace(/\n/gi,"");
	
	f.csv.value=encodeURIComponent(csvOutput);
	f.startDate.value = sDate;
	f.endDate.value = eDate;
	f.submit(); 
};

</script>
</head>

<body oncontextmenu="return false">
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" >
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right"> --%>
	<div id="title_right" class="title">두산 공작기계</div>	
	
	<div id="panel">
		<table id="panel_table" width="100%">
			<tr>
				<td id="menu0" class="menu">샵 레이아웃</td>
			</tr>
			<tr>
				<td id="menu1" class="menu">장비별 가동실적 분석</td>
			</tr>
			<tr>
				<td id="menu2" class="menu">장비 Alarm 발생내역 조회</td>
			</tr>
			<tr>
				<td id="menu3" class="menu">야간 무인 가동 현황 (DMM)</td>
			</tr>
		</table>
	</div>
	<img src="${ctxPath }/images/menu.png" id="menu_btn" >
	
	<table id="jigList" style="color:white; border-collapse: collapse;" border="3" ></table>
					
	<div id="corver"></div>

	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
			<center>
				<table class="mainTable">
					<tr>
						<Td align="center" style=" color:white; font-weight: bolder;" class="title" >
								야간 무인 가동 현황 (DMM)
						</Td>
					</tr>
				</table>
				
				<div style="width: 80%">
					<div class="label" style="float: right">가동기간 <input type="date" class="date" id="sDate"> ~ <input type="date" class="date" id="eDate">
					<span style="background-color: white; color:black; font-weight: bolder; padding: 3px;" class="excel" >엑셀</span> 
					</div>
				</div>
				
				<table style="width: 80%; color: white; text-align: center; border-collapse: collapse;" class="tmpTable" border="1">
					<Tr style="font-weight: bolder;background-color: rgb(34,34,34)" class="thead">
						<td>No</td>
						<td>설비명</td>
						<td>날짜</td>
						<td>정지시간</td>
						<td>상태</td>
						<td>기준 날짜</td>
					</Tr>
				</table>
			</center>
		</div>
	</div>
	
</body>
</html>