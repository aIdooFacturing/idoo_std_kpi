<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}
	
</style>
<title><spring:message code="move_operation"></spring:message></title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<style type="text/css">
	body{
		overflow :hidden;
	}
</style>
<script type="text/javascript">
var shopId = 1;

var save_type = "init"

	function getPrdNo(){
	var url = "${ctxPath}/chart/getMatInfo.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			json = data.dataList;
			
			var option = "<option value='ALL'>전체</option>";
			
			$(json).each(function(idx, data){
				option += "<option value='" + decode(data.prdNo) + "'>" + decode(data.prdNo) + "</option>"; 
			});
			
			$("#group").html(option);
			
			getLotNoByPrdNo();
		}
	});
};

function getLotNoByPrdNo(){
	var url = "${ctxPath}/chart/getLotNoByPrdNo.do";
	var param = "prdNo=" + $("#group").val();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			json = data.dataList;
			
			var option = "<option value='ALL'>전체</option>";
			
			$(json).each(function(idx, data){
				option += "<option value='" + data.lotNo + "'>" + data.lotNo + "</option>"; 
			});
			
			$("#lotNo").html(option);
			
			getLotInfo();
		}
	});
};

$(function(){
	getPrdNo();
	$(".excel").click(csvSend);
	setEl();
	setDate();
	
	$("#menu_btn").click(function(){
		location.href = "${ctxPath}/chart/index.do"
	});
	
	/* $(".date").change(getLotInfo);
	$("#group").change(getLotInfo); */
	
	bindEvt();
});

function bindEvt(){
	$("#save").click(saveRow);	
};

var valueArray = new Array();

var valid = true;
function saveRow(){
	valid = true;
	 for(var i = 0; i < cntArray.length; i++){
		var stock =  cntArray[i][0];
		var sum = 0;
		$("." + cntArray[i][1]).each(function(idx, data){
			sum += Number($(data).val());		
		});
		
		if(stock<sum){
			valid = false;
			alert("${cnt_more_than_stock}");
			$("." + cntArray[i][1] + ":nth(0)").focus();
			return;
		}
	 };
	
	valueArray = [];
	var url = "${ctxPath}/chart/transferOprCnt.do";
	
	$(".contentTr").each(function(idx, data){
		var sendCnt = Number($(data).children("td:nth(4)").children("input").val());
		var prdNo = $(data).children("td:nth(0)").html();
		var date = $(data).children("td:nth(5)").children("input[type='date']").val() + " " + $(data).children("td:nth(5)").children("input[type='time']").val() 
		var oprNm = $(data).children("td:nth(3)").children("select").val();
		
		var obj = new Object();
		
		var preOpr =  $(data).children("td:nth(1)").attr("oprNm");
		/* if(oprNm=="0010"){
			preOpr = "0000";
		}else if(oprNm=="0020"){
			preOpr = "0010";
		}else if(oprNm=="0040"){
			preOpr = "0020";
		}; */
		
		obj.prdNo = prdNo;
		obj.sendCnt = sendCnt;
		obj.date = date;
		obj.oprNm = oprNm; 
		obj.preOpr = preOpr;
		
		if(valid && (sendCnt!=0 || sendCnt!="")) valueArray.push(obj);
	});
	
	var obj = new Object();
	obj.val = valueArray;
	
	var param = JSON.stringify(obj);
	
	$.ajax({
		url : url,
		data :"val=" + param,
		type : "post",
		dataType : "text",
		success : function(data){
			alert("저장되었습니다.")
			getLotInfo();
		}
	}); 
};

var className = "";
var classFlag = true;

function getLotInfo(){
	classFlag = true;
	var url = "${ctxPath}/chart/getOprStockInfo.do";
	
	var param = "prdNo=" + $("#group").val() + 
				"&lotNo=" + $("#lotNo").val();
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			cntArray = [];
			
			var tr = "<thead>" + 
						"<Tr style='background-color:#222222'>" +
							"<Td>" +
								"${mat_prd_no}" +
							"</Td>" +
							"<Td>" +
								"${current_operation}" +
							"</Td>" +
							"<Td>" +
								"${stock}" +
							"</Td>" +
							"<Td width='10%'>" +
								"${operation}" +
							"</Td>" +
							"<Td>" +
								"${move_cnt}" +
							"</Td>" +
							"<Td>" +
								"${release_date}" +
							"</Td>" +
							"<Td>" +
								"${divide_lot}" +
							"</Td>" +  
						"</Tr></thead><tbody>";
						
			$(".alarmTable").html(tr)			
			$(json).each(function(idx, data){
				var array = [data.stockCnt, "stock_" + data.id];
				cntArray.push(array);
				
				if(data.seq!="."){
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
				};
				classFlag = !classFlag;
					
				if(data.stockCnt==0) return;
				var oprNo;
				if(data.oprNm=="0010"){
					oprNo = "R삭";
				}else if(data.oprNm=="0020"){
					oprNo = "MCT 1차";
				}else if(data.oprNm=="0030"){
					oprNo = "MCT 2차";
				}else if(data.oprNm=="0040"){
					oprNo = "CNC 2차";
				}else if(data.oprNm=="0000"){
					oprNo = "자재창고";
				}
				
					tr = "<tr class='contentTr " + className + " parent" + data.id + "' >" +
								"<td id='prdNo" + data.id + "'>" + decodeURIComponent(data.prdNo).replace(/\+/gi, " ") + "</td>" +
								"<td oprNm='" + data.oprNm + "'>" + oprNo + "</td>" + 
								"<td>" + data.stockCnt + "</td>" + 
								"<td ><select id='oprNm" + data.id + "'>" + getOprList + "</select></td>" +
								"<td><input type='text' id='sendCnt" + data.id + "' value='0' class='stock_" + data.id + "'> </td>" +
								"<td><input type='date' id='date" + data.id + "' class='date' style='font-size : " + getElSize(40) + "'><input type='time' id='time" + data.id + "' class='time' style='font-size : " + getElSize(40) + "'></td>" + 
								"<td><button onclick='addNewLot(this);' class='" + data.id + "'>${divide}</button></td>" + 
						"</tr>";	
					$(".alarmTable").append(tr).css({
						"font-size": getElSize(40),
					});
				}
			});
			
			tr = "</tbody>";
			$(".alarmTable").append(tr)
			
			
			$(".alarmTable td").css({
				"padding" : getElSize(20),
				"font-size": getElSize(40),
				"border": getElSize(5) + "px solid black"
			});
			
			$("#wrapper").css({
				"height" :getElSize(1650),
				"width" : "95%",
				"overflow" : "hidden"
			});
			
			$("button, input, select").css({
				"font-size" : getElSize(40)
			})
			
			$("#wrapper").css({
				"margin-left" : (contentWidth/2) -($("#wrapper").width()/2)
			});
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#222222"
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#323232"
			});
			
			$("#wrapper div:last").remove();
			scrolify($('.alarmTable'), getElSize(1450));
			$("#wrapper div:last").css("overflow", "auto");
			
			setToday();
		}
	});
};
 
var getOprList =  "<option value='0010'>R삭</option>" +
				"<option value='0020'>MCT 1차</option>" + 
				"<option value='0040'>CNC 2차</option>"; 
				

var cntArray = [];
function addNewLot(obj){
	var lotName = $(obj).attr("class"); 
	var preOpr = $(obj).parent("td").parent("tr").children("td:nth(1)").attr("oprNm");
	var parent = $(".parent" + lotName)[0];
	
	var className = $(obj).parent("td").parent("tr").children("td:nth(4)").children("input").attr("class");
	var prdNo = $(obj).parent("td").parent("tr").children("td:nth(0)").html();
	var date = "<input type='date'  class='date'><input type='time'  class='time'>";
	var dividLot = "<button onclick='delRow(this);' class='" + lotName + "'>${del}</button>";
	
	
	var tr = "<tr class='contentTr'>" + 
				"<td>" + prdNo + "</td>" + 
				"<td oprNm='" + preOpr + "'> </td>" +
				"<td> </td>" +
				"<td><select>" + getOprList + "</select></td>" +
				"<td><input type='text' value='0' class='" + className + "'></td>" + 
				"<td>" + date + "</td>" + 
				"<td>" + dividLot + "</td>" + 
			"</tr>";
			
	$(parent).after(tr);
	setToday();
	
	$("button, input, select").css({
		"font-size" : getElSize(40)
	})
};

function delRow(el){
	$(el).parent("td").parent("Tr").remove();
}

function getOprNm(prdNo, id){
	var url = "${ctxPath}/chart/getOprNmList.do";
	var rw = prdNo.indexOf("RW");

	if(rw!=-1){
		rw = "true";
	}else{
		rw = "false";
	}
	
	prdNo = prdNo.replace("유럽","UR");
	
	var param = "prdNo=" + prdNo + 
				"&rw=" + rw;
	
	var options="";
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		//async : false,
		success : function(data){
			var json = data.dataList;
			
			$(json).each(function(idx, data){
				options += "<option value='" + data.val + "'>" + data.val + "</option>";
			});
			$("#" + id).html(options)
		}
	});
	
//	return options;
};

function setToday(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	var hour = addZero(String(date.getHours()));
	var minute = addZero(String(date.getMinutes()));
	
	$(".date").val(year + "-" + month + "-" + day);
	$(".time").val(hour + ":" + minute);
};

function getBanner(){
	var url = "${ctxPath}/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100);
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = true;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 100)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},15000, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};
function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		getBanner();
		$("#intro_back").css("display","block");
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};

function cancelBanner(){
	window.localStorage.setItem("banner", "false");
	$("#bannerDiv").css("z-index",-9);
	chkBanner();
};

function addBanner(){
	var url = "${ctxPath}/chart/addBanner.do";
	var param = "msg=" + $("#banner").val() + 
				"&rgb=" + $("#color").val() + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			window.localStorage.setItem("banner", "true");
			$("#bannerDiv").css("z-index",-9);
			location.reload();
			//chkBanner();
		}
	});
};

var panel = false;
function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};


function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function setDate(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	$(".date").val(year + "-" + month + "-" + day);
};


function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
		
	});
	
};

function setEl() {
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight, 
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1
	});

	$(".title_right").css({
		"float" : "right",
	});

	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(130),
		"left" : 0,
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});
	

	$("#panel").css({
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"top" : 0,
		"left" : -contentWidth * 0.2 - getElSize(40),
		"z-index" : 99999
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(40)
	});

	$("#panel_table td").css({
		"padding" : getElSize(30),
		"cursor" : "pointer"
	});

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	
	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});
	
	$("#group, #lotNo, button").css({
		"font-size" : getElSize(50)
	});
	
	$("#jigSelector, .goGraph, .excel, .label").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50),
		"margin-bottom" : 0
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".alarmTable, .alarmTable tr, .alarmTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
	});
	
	$(".alarmTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100),
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(60)
	});
	
	$(".machineListForTarget").css({
		"position" : "absolute",
		"width" : getElSize(1200),
		"height" : getElSize(1200),
		"overflow" : "auto",
		//"top" : getElSize(50),
		//"background-color" : "rgb(34,34,34)",
		"background-color" : "green",
		"color" : "white",
		"font-size" : getElSize(50),
		"padding" : getElSize(50),
		"overflow" : "auto",
		"border-radius" : getElSize(50),
		"border" : getElSize(10) + "px solid white",
	});
	
	$(".machineListForTarget").css({
		"left" : (originWidth/2) - ($(".machineListForTarget").width()/2),
	});
	
	$("#bannerDiv").css({
		"position" : "absolute",
		"width" : getElSize(1500),
		"height" : getElSize(200),
		"border-radius" : getElSize(20),
		"padding" : getElSize(50),
		"background-color" : "lightgray",
		"z-index" : -9
	});

	
	$("#bannerDiv").css({
		"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
		"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
	});
	
	$("#bannerDiv input[type='text']").css({
		"width" : getElSize(1200),
		"font-size" : getElSize(50)
	});
	
	$("#bannerDiv button").css({
		"margin" : getElSize(50),
		"font-size" : getElSize(50)
	});
	
	$("#intro").css({
		/* "width" : contentWidth, */
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.3,
		"position" : "absolute",
		"background-color" : "white",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	});
	
	$("#addBtn").css({
		"margin-top" : getElSize(50)	
	}).click(addRow);
	
	chkBanner();
};

function addRow(){
	if(classFlag){
		className = "row2"
	}else{
		className = "row1"
	};
	classFlag = !classFlag;
	
	var tr = "<tr class='" + className + "'>" +
				"<td>" + "</td>"+
				"<td>" + "</td>"+ 
				"<td>" + "</td>"+ 
				"<td>" + "</td>"+ 
				"<td>" + "</td>"+  
				"<td>" + "</td>"+
				"<td><input type='text'></td>"+
				"<td><input type='text'></td>" +  
				"<td>" + "</td>"+ 
				"<td>" + "</td>"+ 
			"</tr>";
	
	$("#wrapper table:last tbody").append(tr);
	
	$(".row1").not(".tr_table_fix_header").css({
		"background-color" : "#222222"
	});

	$(".row2").not(".tr_table_fix_header").css({
		"background-color": "#323232"
	});
	
	$("#table td").css({
		"padding" : getElSize(20),
		"font-size": getElSize(40),
		"border": getElSize(5) + "px solid black"
	});
};

var csvOutput;
function csvSend(){
	var sDate, eDate;
	
	sDate = $("#sDate").val();
	eDate = $("#eDate").val();
	
	csvOutput = csvOutput.replace(/\n/gi,"");
	
	f.csv.value=encodeURIComponent(csvOutput);
	f.startDate.value = sDate;
	f.endDate.value = eDate;
	f.submit(); 
};

</script>
</head>

<body >
	<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
	
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right"> --%>
	<div id="title_right" class="title"></div>	
	
	<img src="${ctxPath }/images/home.png" id="menu_btn" >
	
					
	<div id="corver"></div>

	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
				<table class="mainTable">
					<tr>
						<Td align="center" style=" color:white; font-weight: bolder;" class="title" >
							<spring:message code="move_operation"></spring:message>
						</Td>
					</tr>
				</table>
			
				<div style="width: 95%">
					<table class="label" style="float: right;">
						<tr>
							<Td><spring:message code="prd_no"></spring:message>&nbsp;</td><Td><select id="group"></select></Td>
							<!-- <Td>&nbsp;&nbsp;로트번호&nbsp;</td><Td><select id="lotNo"></select></Td> -->
						</tr>
						<tr>
							<Td colspan="4" style="text-align: right;"><button  onclick="getLotInfo()"><spring:message code="check"></spring:message></button><button id="save"><spring:message code="save"></spring:message></button></Td>
						</tr>
					</table>
				</div>
				<div id="wrapper">
					<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" border="1" id="table">
						
					</table>
				</div>
		</div>
	</div>
</body>
</html>