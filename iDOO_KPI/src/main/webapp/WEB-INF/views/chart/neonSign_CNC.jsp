<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/demo.css">
<link rel="stylesheet" href="${ctxPath }/css/style.css">

<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

<style>

@font-face {font-family:MalgunGothic; src:url(../fonts/malgun.ttf);}

*{
	font-family:'MalgunGothic';
}

#container{
	background-color: black;
}

body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style>
<script type="text/javascript">
	var shopId = 1;
	
	$(function(){
		setEl();
		setTime();
		getDvcId();
	});
	
	var dvcArray = [];
	var alarmDvcArray =[];
	var dvcIdx = 0;
	var alarmIdx = 0;
	function getDvcId(){
		var url = ctxPath + "/chart/getDvcId.do";
		var param = "shopId=" + shopId + 
					"&jig=CNC";
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				dvcArray = [];
				alarmDvcArray = [];
				$(json).each(function(idx, data){
					if(data.status=="ALARM"){
						alarmDvcArray.push(data.dvcId);
					}else{
						dvcArray.push(data.dvcId)	
					}
				});
				
				
				if(alarmDvcArray.length==0){
					alarmIdx = 0;
					getData(dvcArray[dvcIdx]);
					
					dvcIdx++;
					if(dvcIdx>=dvcArray.length) dvcIdx = 0;
				}else{
					getData(alarmDvcArray[alarmIdx]);
					alarmIdx++;
					if(alarmIdx>=alarmDvcArray.length) alarmIdx = 0;
				}
				
				setTimeout(getDvcId, 5000)
				//getData(dvcArray[0]);
			}
		});
	};
	
	var idx = 1;
	var alarm_interval = true;
	function alarmAnim(){
		alarm_interval = setInterval(function(){
			if($("#container").css("background-color")=="rgb(0, 0, 0)"){
				$("#container").css("background-color", "rgb(255, 0, 0)");
			}else{
				$("#container").css("background-color", "rgb(0, 0, 0)");
			}
		}, 500);
	};
	
	function getData(dvcId){
		clearInterval(alarm_interval);
		$("#container").css("background-color", "rgb(0, 0, 0)");
		
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var today = year + "-" + month + "-" + day;
		
		var url = ctxPath + "/chart/getData.do";
		var param = "dvcId=" + dvcId +
					"&sDate=" + today;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var dvcName = data.name;
				
				var alarm = data.lastAlarmCode + "-" + data.lastAlarmMsg;
				var color;
				
				var status = "";
				if(data.status=="IN-CYCLE"){
					alarm = "정 상 가 동";
					color = "#329C00";
				}else if(data.status=="WAIT"){
					alarm = "대  기  중";
					color = "yellow";
				}else if(data.status=="ALARM"){
					alarm = data.lastAlarmCode + "-" + data.lastAlarmMsg;
					color = "yellow";
					alarmAnim();
				}else if(data.status=="NO-CONNECTION"){
					alarm = "전 원 무";
					color = "gray";
				}
				
				var opRatioColor = "";
				if(data.targetRatio>=95){
					opRatioColor = "#329C00";
				}else{
					opRatioColor = "red";
				}
				
				$("#dvcName").html(dvcName);
				$("#targetCnt").html(data.tgCnt).css("color","white");
				$("#currentCnt").html(data.cntCyl).css("color",opRatioColor);
				$("#targetRatio").html(data.targetRatio + "%").css("color",opRatioColor);
				$("#alarm").html(alarm).css({
					"color" : color,
					"font-weight" : "bolder"
				});
				
				
					
				//$("table").css("border", getElSize(15) + "px solid " + status)
				
				/* setTimeout(function(){
					getData(dvcArray[idx])
					idx++;
					
					if(idx == (dvcArray.length)) idx = 0;					
				}, 5000); */
			},
			error : function(e1,e2,e3){
				console.log(e1,e2,e3)
			}
		});
	};
	
	function addZero(str){
		if(str.length==1){
			str = "0" + str;
		};
		return str;
	};
	
	function setTime(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		
		var today = year + ". " + month + ". " + day + " " + hour + ":" + minute; 
		
		$("#testDrive").html(today).css({
			"font-size" : getElSize(200) + "px",
			"font-weight" : "bolder"
		});
		
		$(".neon").css({
			"font-size" : getElSize(220) + "px",
			//"text-shadow" : "0 0 " + getElSize(20) + "px #fff, 0 0 " + getElSize(30) + "px #fff, 0 0 " + getElSize(20) + "px #fff, 0 0 " + getElSize(20) + "px #ff0000, 0 0 " +getElSize(20) + "px #ff0000, 0 0 " + getElSize(120) + "px #ff0000, 0 0 " +getElSize(120) + "px #ff0000, 0 0 " + getElSize(150) + "px #ff0000",
			"text-shadow" :  getElSize(-10) + "px 0 black, 0 " + getElSize(10) + "px black, " + getElSize(10) + "px 0 black, 0 " + getElSize(-10) + "px black",
 
			"margin-top" : getElSize(60),
		});
		
		$("#testDrive").css({
			//"text-shadow" : "0 0 " + getElSize(20) + "px #fff, 0 0 " + getElSize(30) + "px #fff, 0 0 " + getElSize(20) + "px #fff, 0 0 " + getElSize(20) + "px #ff0000, 0 0 " +getElSize(20) + "px #ff0000, 0 0 " + getElSize(120) + "px #ff0000, 0 0 " +getElSize(120) + "px #ff0000, 0 0 " + getElSize(150) + "px #ff0000",
		})
		
		$("#alarm").css({
			"text-shadow" :  getElSize(-10) + "px 0 black, 0 " + getElSize(10) + "px black, " + getElSize(10) + "px 0 black, 0 " + getElSize(-10) + "px black",
			"font-size" : getElSize(130),
			"font-weight" : "bolder"
		});
		
		setTimeout(setTime, 1000);
	};
	
	function setEl(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#container").css({
			/* "width" : contentWidth,
			"height" : contentHeight, */
			"width" : originWidth,
			"height" : originHeight,
			/* "margin-left" : marginWidth,
			"margin-top" : marginHeight */
		});
		
		$("*").css({
			"font-size" : getElSize(240) + "px",
			"color" : "white",
			"font-weight" : "bolder"
		});
		
		$(".div").css({
			"margin-top" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$(".neon").css({
			"float" : "right",
			"color" : "red",
			"margin-right" : getElSize(400)
		});
		
		$("#testDrive").css({
			"color" : "yellow"
		});
		
		$(".title").css({
			"float" : "left",
			"margin-left" : getElSize(400)
		});
		
		$("#targetRatio").css({
			"margin-left" : -getElSize(550)
		});
		
		$("table").css({
			"border-radius" : getElSize(20),
			"height" : originHeight
		});
		
		$("table td").css({
			"padding" : getElSize(25)
		});
		
		
		$("table td").css({
			"border" : getElSize(10) + "px solid yellow"
		});
	};
</script>
</head>
<body>
	<div id="container">
		<div id="wrapper">
			<center>
				<table style="width: 100%; border-collapse: collapse;">
					<Tr>
						<Td style="text-align: center;">
							<div id="testDrive" class="icon- testDrive"></div>
							<span id="dvcName"> - </span>
						</Td>
					</Tr>
					<Tr>
						<Td style="text-align: center;vertical-align: middle;"><span class="title">목 표 수 량 </span><span class="icon- neon" id="targetCnt">0</span> </Td>
					</Tr>
					<Tr>
						<Td style="text-align: center;vertical-align: middle;"><span class="title" >생&nbsp;산&nbsp;수&nbsp;량</span> <span class="icon- neon" id="currentCnt">0</span></Td>
					</Tr>
					<Tr>
						<Td style="text-align: center;vertical-align: middle;"><span class="title" >달&nbsp;&nbsp;&nbsp;성&nbsp;&nbsp;&nbsp;율</span> <span class="icon- neon" id="targetRatio">0</span></Td>
					</Tr>
					<Tr>
						<Td style="text-align: center;"><span  id="alarm">-</span></Td>
					</Tr>
				</table>
			</center>
		</div>
	</div>
</body>
</html>