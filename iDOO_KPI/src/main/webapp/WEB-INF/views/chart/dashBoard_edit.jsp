<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;
	
	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/svg_controller_edit.js"></script>

<%-- <link rel="stylesheet" href="${ctxPath }/css/canvas.css"> --%>
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_draggable.js"></script>
<style>
.chartTitle{
	position : absolute;
	top:-100;
	left:20%;
	right:0;
	margin:auto;
	font-size : 50;
}

#chart{
	overflow: hidden;
}

#container{
	background: url("../images/DashBoard/Background.png");
	background-size : 100% 100%;
}

body{
	background-color: black;
	overflow: hidden;
}

.title{
	top : 50px;
	position: absolute;
	z-index: 99;
}

#title_left{
	left : 50px;
	width: 300px;
}

#title_right{
	right: 50px;
	width: 300px;
}

#pieChart1{
	position: absolute;
	left : 800px;
	z-index: 99;
	top: 1480px;
	height: 700px;
	width: 750px;
}

#pieChart2{
	position: absolute;
	z-index: 99;
}

#tableDiv{
	left: 20px;
	width: 600px; 
	position: absolute; 
	z-index: 999;
	top: 450px;
}

#table{
	border: 1px solid white;
	border-collapse: collapse;
	z-index: 999;
	background-color: black;
}

.tr{
	font-size: 30px;
}

.td{
	padding: 10px;
	border: 1px solid white; 
}

#textBackground{
	background-color: white;
	width: 780px;
	height: 320px;
	z-index: 1;
	position: absolute;
	border-radius : 10px;
	opacity : 1;
	top: 1720;
	left: 2570;
}
#mainText{
	position: absolute;
	z-index: 2;
	font-size: 70px;
	font-weight: bolder;
	text-align: right;
	top: 1730;
	left: 2610;
}
</style> 
<script type="text/javascript">
function drawGroupLine(){
	ctx.lineWidth = getElSize(5);
	ctx.strokeStyle = "#ffffff";
	
	// YF/FRT CNC
	//ctx.rect(getElSize(120), getElSize(200), getElSize(390), getElSize(680));
	ctx.rect(getElSize(310), getElSize(200), getElSize(390), getElSize(680));
	var text = document.createTextNode("YP FRT (CNC)")
	var div = document.createElement("div");
	div.appendChild(text);
	
	div.appendChild(document.createElement("br"));
	div.appendChild(document.createTextNode("내수, 북미"));
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"font-size : " + getElSize(40) + ";" + 
						'text-align :center;' +
						"z-index : 999"; 
	
	$("#container").prepend(div);
	
	$(div).css({
		"top" : getElSize(100) + marginHeight,
		//"left" : marginWidth + getElSize(180)
		"left" : marginWidth + getElSize(380)
	});
	
	// YF/FRT CNC
	ctx.rect(getElSize(800), getElSize(200), getElSize(390), getElSize(680));
	var text = document.createTextNode("UM FRT (CNC)");
	var div = document.createElement("div");
	div.appendChild(text);
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"font-size : " + getElSize(40) + ";" + 
						"z-index : 999"; 
	
	$("#container").prepend(div);
	
	$(div).css({
		"top" : getElSize(150) + marginHeight,
		"left" : getElSize(860) + marginWidth
	});
	
	//TA/RR
	ctx.rect(getElSize(1325), getElSize(200), getElSize(390), getElSize(830));
	var text = document.createTextNode("TA RR");
	var div = document.createElement("div");
	div.appendChild(text);
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"font-size : " + getElSize(40) + ";" + 
						"z-index : 999"; 
	
	$("#container").prepend(div);
	
	$(div).css({
		"top" : getElSize(150) + marginHeight,
		"left" : getElSize(1455) + marginWidth
	});
	
	//TA/FRT
	ctx.rect(getElSize(1900), getElSize(200), getElSize(390), getElSize(830));
	var text = document.createTextNode("TA FRT");
	var div = document.createElement("div");
	div.appendChild(text);
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"font-size : " + getElSize(40) + ";" + 
						"z-index : 999";
	
	$("#container").prepend(div);
	
	$(div).css({
		"top" : getElSize(150) + marginHeight,
		"left" : getElSize(2030) + marginWidth
	});
	
	//LFA/RR
	ctx.rect(getElSize(2480), getElSize(200), getElSize(390), getElSize(830));
	var text = document.createTextNode("JC RR");
	var div = document.createElement("div");
	div.appendChild(text);
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"font-size : " + getElSize(40) + ";" + 
						"z-index : 999"; 
	
	$("#container").prepend(div);
	
	$(div).css({
		"top" : getElSize(150) + marginHeight,
		"left" : getElSize(2605) + marginWidth
	});
	
	ctx.rect(getElSize(20), getElSize(200), getElSize(190), getElSize(680));
	var text = document.createTextNode("LFA RR");
	var div = document.createElement("div");
	div.appendChild(text);
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"font-size : " + getElSize(40) + ";" + 
						"z-index : 999"; 
	
	$("#container").prepend(div);
	
	$(div).css({
		//"top" : getElSize(650) + marginHeight,
		//"left" : getElSize(2505) + marginWidth
		"left" : getElSize(40) + marginWidth,
		"top" : getElSize(150) + marginHeight,
	});
	
	//HR/FRT.PU
	ctx.rect(getElSize(3060), getElSize(200), getElSize(390), getElSize(830));
	var text = document.createTextNode("HR PU / FRT");
	var div = document.createElement("div");
	div.appendChild(text);
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"font-size : " + getElSize(40) + ";" + 
						"z-index : 999";  
	
	$("#container").prepend(div);
	
	$(div).css({
		"top" : getElSize(150) + marginHeight,
		"left" : getElSize(3150) + marginWidth
	});
	
	//YD RR
	ctx.rect(getElSize(1325), getElSize(1070), getElSize(390), getElSize(1080));
	/* var text = document.createTextNode("YD RR");
	var div = document.createElement("div");
	div.appendChild(text);
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"font-size : " + getElSize(40) + ";" + 
						"z-index : 999"; 
	
	$("#container").prepend(div);
	
	$(div).css({
		"top" : getElSize(1080) + marginHeight,
		"left" : getElSize(1490) + marginWidth
	}); */
	
	var text = document.createTextNode("FS RR");
	var div = document.createElement("div");
	div.appendChild(text);
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"font-size : " + getElSize(40) + ";" + 
						"z-index : 999"; 
	
	$("#container").prepend(div);

	$(div).css({
		"top" : getElSize(1100) + marginHeight,
		"left" : getElSize(1535) + marginWidth
	});
	
	//YP RR
	ctx.rect(getElSize(1900), getElSize(1070), getElSize(390), getElSize(1080));
	
	var text = document.createTextNode("YP RR 북미");
	var div = document.createElement("div");
	div.appendChild(text);
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"font-size : " + getElSize(40) + ";" + 
						"z-index : 999"; 
	
	$("#container").prepend(div);
	
	$(div).css({
		"top" : getElSize(1500) + marginHeight,
		"left" : getElSize(1910) + marginWidth 
	});
	
	var text = document.createTextNode("UM RR 내수");
	var div = document.createElement("div");
	div.appendChild(text);
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"font-size : " + getElSize(40) + ";" + 
						"z-index : 999"; 
	
	$("#container").prepend(div);
	
	$(div).css({
		"top" : getElSize(1220) + marginHeight,
		"left" : getElSize(2055) + marginWidth
	});
	
	//LFA RR, MCT
	ctx.rect(getElSize(2480), getElSize(1070), getElSize(390), getElSize(1080));
	var text = document.createTextNode("UM "); //RR 유럽
	var div = document.createElement("div");
	div.appendChild(text);
	
	//div.appendChild(document.createElement("br"));
	div.appendChild(document.createTextNode("RR"));
	div.appendChild(document.createElement("br"));
	div.appendChild(document.createTextNode("유럽"));
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"font-size : " + getElSize(40) + ";" + 
						"text-align : center;" +
						"z-index : 999"; 
	
	$("#container").prepend(div);
	
	$(div).css({
		/* "top" : getElSize(1750),
		"left" : getElSize(2395) + marginWidth */
		"top" : getElSize(1350),
		"left" : getElSize(2490) + marginWidth
	});
	
	var text = document.createTextNode("LFA RR")
	var div = document.createElement("div");
	div.appendChild(text);
	
	div.appendChild(document.createElement("br"));
	div.appendChild(document.createTextNode("(MCT)"));
	
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"font-size : " + getElSize(40) + ";" + 
						"z-index : 999"; 
	
	$("#container").prepend(div);
	
	$(div).css({
		//"top" : getElSize(1100),
		//"left" : getElSize(2545) + marginWidth
		"text-align" : "center",
		"left" : getElSize(2710) + marginWidth,
		"top" : getElSize(1260),
	});
	
	//YP RR
	ctx.rect(getElSize(3060), getElSize(1070), getElSize(390), getElSize(1080));
	var text = document.createTextNode("YP RR");
	var div = document.createElement("div");
	div.appendChild(text);
	
	div.appendChild(document.createElement("br"));
	div.appendChild(document.createTextNode("내수"));
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"font-size : " + getElSize(40) + ";" + 
						"text-align : center;" +
						"z-index : 999"; 
	
	$("#container").prepend(div);
	
	$(div).css({
		"top" : getElSize(1075),
		"left" : getElSize(3075) + marginWidth
	});
	
	var text = document.createTextNode("YP FRT");
	var div = document.createElement("div");
	div.appendChild(text);
	
	div.appendChild(document.createElement("br"));
	div.appendChild(document.createTextNode("(MCT)"));
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"text-align :center;"+
						"font-size : " + getElSize(40) + ";" + 
						"z-index : 999"; 
	
	$("#container").prepend(div);
	
	$(div).css({
		"top" : getElSize(1075),
		"left" : getElSize(3260) + marginWidth
	});
	//TQ FRT
	ctx.rect(getElSize(3580), getElSize(200), getElSize(220), getElSize(1960));
	var text = document.createTextNode("TQ FRT");
	var div = document.createElement("div");
	div.appendChild(text);
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"font-size : " + getElSize(40) + ";" +
						"text-align : center;" + 
						"z-index : 999"; 
	
	$("#container").prepend(div);
	
	$(div).css({
		"top" : getElSize(150),
		"left" : getElSize(3620) + marginWidth
	});
	
	var text = document.createTextNode("UM FRT");
	var div = document.createElement("div");
	div.appendChild(text);
	
	div.appendChild(document.createElement("br"));
	div.appendChild(document.createTextNode("(MCT)"));
	
	div.style.cssText = "position :absolute;" + 
						"color : white;" + 
						"font-size : " + getElSize(40) + ";" + 
						"text-align : center;" + 
						"z-index : 999"; 
	
	$("#container").prepend(div);
	
	$(div).css({
		"top" : getElSize(1250),
		"left" : getElSize(3620) + marginWidth
	});
	
	ctx.stroke();
};

	var loopFlag = null;
	var flag = false;
	
	var canvas;
	var ctx;
	
	
	$(function(){
		canvas = document.getElementById("canvas");
		ctx = canvas.getContext("2d");
		canvas.width = contentWidth;
		canvas.height = contentHeight;
		$("#canvas").css({
			"z-index" : 7,
			"position" : "absolute",
			"top" : marginHeight,
			"left" : marginWidth
		});
		
		setDivPos();
		setInterval(time, 1000);
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				//location.reload();
			};
		},1000*10);
		
		//getsMachineStatus();
		
		drawGroupLine();
		$("#delDevice").click(showDelBtn);
		$("#addDevice").click(showTemplate);
	});
	
	function showTemplate(){
		$("#tmplBox").css("z-index",9999999);
		$("#close").css("z-index",99999999);
		
		getAllTemplate();
	};
	
	function getAllTemplate(){
		var url = "${ctxPath}/chart/getTemplate.do";
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				$("#tmplBox").empty();
				var json = data.dataList;
				
				$(json).each(function(idx, data){
					var viewBox = data.viewBox;
					var transform = data.transform;
					var d = data.d;
					var id = "T_" + data.id;
					
					SVG_T(viewBox, transform, d, id);
				});
			}
		});
	};
	
	function SVG_T(viewBox, transform, d, id){
		var svg=document.createElementNS(NS,"svg");
		svg.setAttribute("id", id);
		svg.setAttribute("width", 150);
		svg.setAttribute("height", 150);
		svg.setAttribute("viewBox", viewBox);
		
		svg.style.cssText = "margin : 10px";
	
		document.getElementById("tmplBox").appendChild(svg);	
		$("#tmplBox").css("text-align","center");
		path_T(d, transform, id);
	};
	
	function path_T(d, transform, id){
		var obj=document.createElementNS(NS,"path");
		obj.setAttribute("d", d);
		obj.setAttribute("transform", transform);
		obj.setAttribute("fill", "gray");
		
		obj.style.cssText = "cursor : pointer";
				
		document.querySelector('#' + id).appendChild(obj);
		
		$(obj).hover(function(){
			var $path = document.getElementById(id);
			$path.setAttribute("stroke",'red');
			$path.setAttribute("stroke-width","2");
			
			
		}, function(){
			var $path = document.getElementById(id);
			$path.removeAttribute("stroke");
			$path.removeAttribute("stroke-width");
		})
		
		$(obj).click(function(){
		//	hideTemplate();
			addMachine(id.substr(2));
		});
	//	scale(id, 150, 150);
	};
	
	function addMachine(){
			
	};
	
	var toggle = false;
	function showDelBtn(){
		if(toggle){
			$(".del_btn").remove();
		}else{
			$("image").each(function(idx, data){
				var del_btn = document.createElement("img");
				del_btn.setAttribute("src", ctxPath + "/images/del.png");
				del_btn.setAttribute("class", "del_btn");
				
				del_btn.style.cssText = "width:" + getElSize(0) + "px;" +
										"height : " + getElSize(0) + "px;" + 
										"position : absolute;" +
										"z-index : 999999;" + 
										"cursor : pointer;" + 
										"top :" + ($(data).offset().top - getElSize(30)) + "px;" +
										"left :" + ($(data).offset().left + $(data).width()) + "px;";
				
				$("body").append(del_btn);
				
				$(del_)
			});			
		}
		toggle = !toggle;
	};
	
	function getNightlyMachineStatus(){
		var url = "${ctxPath}/device/getNightlyMachineStatus.do";
		var date =  new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var targetDate = year + "-" + month + "-" + day;
		var param = "targetDate=" + targetDate;
		
		$.ajax({
			url : url,
			data: param,
			dataType : "text",
			type : "post",
			success : function(data){
				var json = $.parseJSON(data);
				
				var tr = "<tr style='font-size: 30px; font-weight: bold;' class='tr' >" + 
							"<td class='td'>No</td>" + 
							"<td class='td'>설비명</td>" + 
							"<td class='td'>날짜</td>" + 
							"<td class='td'>정지시간</td>" + 
							"<td class='td'>상태</td>" + 
						"</tr>";
				csvData = "NO., 설비명, 날짜, 정지시간, 상태LINE";
				var status = "";
				var fontSize = getElSize(25);
				var backgroundColor = "";
				var fontColor = "";
				
				$(json).each(function (idx, data){
					if(data.status=="WAIT" || data.status=="NO-CONNECTION"){
						status = "완료";
						fontColor = "black";
						backgroundColor = "yellow";
					}else if(data.status=='ALARM'){
						status = "중단";
						fontColor = "white";
						backgroundColor = "red";
					}else if(data.status=='IN-CYCLE'){
						status = "가동";
						fontColor = "white";
						backgroundColor = "green";
					};
					
					if(data.isOld=="OLD"){
						fontColor = "white";
						backgroundColor = "black";
					};
					
					tr += "<tr class='contentTr'>" +
							"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + (idx+1) + " </td>" +
							"<td style='border:1px solid white;font-Size:" + fontSize + "px;'> " + data.name + "</td>" +
							"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + data.stopDate + "</td>" +
							"<td style='border:1px solid white;font-Size:" + fontSize + "px;'>" + data.stopTime + "</td>" + 
							"<td style='border:1px solid white;font-Size:" + fontSize + "px; color:" + fontColor + "; background-color:" + backgroundColor + "'>" + status + "</td>" +
						"</tr>";
					csvData += (idx+1) + "," +
								data.name + "," + 
								data.stopDate + "," + 
								data.stopTime + "," + 
								status + "LINE";
				});
				
				if(json.length==0){
					tr = "<tr  style='border:1px solid white; font-Size:" + fontSize + "px;'>" + 
							"<td colspan='5'>없음</tb>" + 
						"<tr>";
				};
				
				$("#table").html(tr);
				$(".tr").css("font-size", getElSize(30));
				$(".td").css("padding", getElSize(10));
				
				setTimeout(function(){
					getNightlyMachineStatus();					
				}, 1000*60*1);
			}
		});
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setDivPos(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#container").css({
			"width": contentWidth,
			"height" : contentHeight,
			"margin-top" : height/2 - ($("#container").height()/2)
		});
		
		$("#container").css({
			"margin-top" : height/2 - ($("#container").height()/2),
			"margin-left" : width/2 - ($("#container").width()/2),
		});
		
		$("#Legend").css({
			"bottom" : 50,
			"left" : width/2 - $("#Legend").width()/2	
		});
		
		$("#title_main").css({
			"font-size" : getElSize(100),
			"top" : $("#container").offset().top + (getElSize(10)),
			"color" : "white",
			"font-weight" : "bolder"
		});

		$("#title_main").css({
			"left" : originWidth/2 - ($("#title_main").width()/2)
		});
		
		$("#title_left, #title_right").css({
			"width" : getElSize(300),
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#title_left").css({
			"left" : $("#container").offset().left + getElSize(50)
		});
		
		$("#title_right").css({
			"right" : $("#container").offset().left + getElSize(50)
		});
		
		$("#tableDiv").css({
			"left" : $("#container").offset().left + getElSize(20),
			"width" : getElSize(600),
			"top" : $("#container").offset().top + getElSize(450)
		});
		
		$("#tableTitle").css("font-size", getElSize(40));
		$(".tr").css("font-size", getElSize(30));
		$(".td").css("padding", getElSize(10));
		
		$("#time").css({
			"top": $("#container").offset().top + getElSize(170),
			"right": $("#container").offset().left + getElSize(50),
			"font-size" : getElSize(30)
		});
		
		$("#date").css({
			"top": $("#container").offset().top + getElSize(170),
			"right": $("#container").offset().left + getElSize(250),
			"font-size" : getElSize(30)
		});
		
		
		$("#Legend").css({
			"font-size" : getElSize(50),
			"bottom" : $("#container").offset().top + getElSize(50),
			"left" : originWidth/2 - ($("#Legend").width()/2)
		});
		
		$("#Legend").css("left" , originWidth/2 - ($("#Legend").width()/2));
		
		$("#svg").css({
			"position" : "absolute",
			"width" : contentWidth,
			"height" : contentWidth,
			"z-index" : 9999,
			"top" :0,
			"left" : marginWidth
		});
		
		$("#addDevice, #updateBgImg, #delDevice").css({
			"float" : "right",
			"margin-top" : getElSize(20),
			"position" : "absolute",
			"z-index" : 999999
		});
		
		$("#updateBgImg").css({
			"right" : getElSize(50) + marginWidth		
		}).click(function(){
			$("#file").click()
		})
		
		$("#delDevice").css({
			"right" : getElSize(320) + marginWidth		
		})
		
		$("#addDevice").css({
			"right" : getElSize(500) + marginWidth		
		});
		
		$("#tmplBox").css({
			"overflow" : "auto",
			"border-radius" : getElSize(50) + "px",
			"border" : getElSize(10) + "px solid gray",
			"width" : getElSize(2500),
			"height" : getElSize(1500),
			"background-color" : "white",
			"position" : "absolute",
			"z-index" : -99999
		});
		
		$("#tmplBox").css({
			"left" : (window.innerWidth/2) - ($("#tmplBox").width()/2),
			"top" : (window.innerHeight/2) - ($("#tmplBox").height()/2)
		});
		
		$("#close").css({
			"position" : "absolute",
			"width" : getElSize(100),
			"top" : $("#tmplBox").offset().top - getElSize(50),
			"z-index": -9,
			"cursor" : "pointer",
			"left" : ($("#tmplBox").offset().left + $("#tmplBox").width()) - getElSize(50)
		});
		
		$("#close").click(function(){
			$("#close, #tmplBox").css("z-index", -999)
		});
	};
	
	function bodyNeonEffect(color) {
		var lineWidth = 70;
		var toggle = true;
		setInterval(function() {
			$("#container").css("box-shadow",
					"inset 0px 0px " + lineWidth + "px " + color);

			if (toggle) {
				lineWidth -= 2;
				if (lineWidth == 0) {
					toggle = false;
				}
			} else if (!toggle) {
				lineWidth += 2;
				if (lineWidth == 70) {
					toggle = true;
				}
			};
		},50);
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
	};
	
</script>
<style type="text/css">
	body{
		padding: 0px;
		margin: 0px;
	}
	#container{
		background-color: white;
	}
</style>
</head>
<body >
	<img alt="" src="${ctxPath }/images/close_btn.png" id="close">
	<div id="tmplBox">

	</div>
	<input type="file" id="file"  style="display: none">
	<div id="container" >
		<!-- <button id="updateBgImg">배경 이미지 변경</button>
		<button id="delDevice">장비 삭제</button>
		<button id="addDevice">장비 추가</button> -->  
		
		<div id="svg"></div>
	</div>

	<!-- <div id="pieChart1"  ></div> -->
	<div id="pieChart2" ></div>
	
	<canvas id="canvas"></canvas>

	<%-- <div id="title_main" class="title"><spring:message code="layout"></spring:message> </div> --%>
	<font id="date"></font>
	<font id="time"></font>
</body>
</html>