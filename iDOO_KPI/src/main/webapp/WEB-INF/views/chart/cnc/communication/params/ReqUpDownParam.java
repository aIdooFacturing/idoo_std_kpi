package com.unomic.cnc.communication.params;

import com.unomic.cnc.StringUtils;

public class ReqUpDownParam implements Params
{
	private int type;
	private String name;
	private int size;
	
	public int getType()
	{
		return type;
	}
	public void setType(int type)
	{
		this.type = type;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		name = StringUtils.resize(name, 96);
		this.name = name;
	}
	public int getSize()
	{
		return size;
	}
	public void setSize(int size)
	{
		this.size = size;
	}
	@Override
	public String resizeString()
	{
		return null;
	}
	
	@Override
	public int getMessageLength()
	{
		/*
		try
		{
			return 4 + name.getBytes("MS949").length + 4;
		} 
		catch (UnsupportedEncodingException e)
		{
		}
		*/
//		return 4 + name.getBytes().length + 4;
		return 101;
	}
}
