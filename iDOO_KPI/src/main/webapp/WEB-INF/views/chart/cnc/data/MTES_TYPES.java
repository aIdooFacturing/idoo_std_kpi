package com.unomic.cnc.data;


public interface MTES_TYPES 
{
	// EnAlarmType
	public static final short MTES_NC	= 0;
	public static final short MTES_PC 	= 1;
	
	// EnFeedType
	public static final int MTES_MM_PER_MIN 		= 0;
	public static final int MTES_MM_PER_REVOLUTION 	= 1;
	
	// EnNCStatus
	public static final String MTES_RUN 	= "RUN";
	public static final String MTES_STR     = "STR";
	public static final String MTES_STRT    = "STRT";
	public static final String MTES_STARS   = "****";
	public static final String MTES_STOP 	= "STOP";
	public static final String MTES_HALT 	= "HALT";
	public static final String MTES_RESET 	= "RESET";
	public static final String MTES_UNKNOWN = "UNKNOWN";
	
	// EnNCMode
	public static final String MTES_DNC 	= "DNC";
	public static final String MTES_AUTO 	= "AUTO";
	public static final String MTES_MDI		= "MDI";
	public static final String MTES_EDIT	= "EDT";
	public static final String MTES_RTN		= "RTN";
	public static final String MTES_JOG		= "JOG";
	public static final String MTES_HANDLE	= "HANDLE";
	public static final String MTES_ATC		= "ATC";

}
