<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	/* overflow : hidden; */
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;
	
	$(function(){
		$("#next").click(nextPage)
		$("#prev").click(prevPage)
		
		setStartTime();
		getDvcIdList();
		createNav("monitor_nav", 4);
		
		setEl();
		setDate();	
		time();
		
		setEvt();
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function setEvt(){

	};
	
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"display" : "none",
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

	
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$(".status").css({
			"width" : getElSize(1600),
			"height" : getElSize(170),
			"margin-bottom": -contentHeight/(targetHeight/10)	
		});
		
		$("#prev, #next").css({
			"width" : getElSize(100),
			"margin" : getElSize(50),
			"background-color" : "white",
			"border-radius" : "50%",
			"cursor" : "pointer"
		});
		
		$("img").css({
			"display" : "inline"
		})
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function getDvcIdList(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));

		var sDate = year + "-" + month + "-" + day;
		var eDate = year + "-" + month + "-" + day + " 23:59:59"; 
		
		var param = "sDate=" + sDate + 
					"&eDate=" + eDate + 
					"&shopId=" + shopId + 
					"&maxRow=" + max_row + 
					"&offset=" + ((c_page-1)*max_row);
		
		var url = "${ctxPath}/chart/getBarChartDvcId.do";
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dvcId;
				if(json.length>=max_row){
					$("#next").addClass("enablePointer");
				}else{
					$("#next").removeClass("enablePointer");
				}
				
				for(var i = 0; i < json.length; i++){
					drawBarChart2("status2_" + i, json[i].name);	
					getStatusChart2(json[i].dvcId, i);
				}
			}
		});
	};
	
	var c_page = 1;
	var max_row = 20;
	
	function getStatusChart2(dvcId, idx){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth() + 1));
		var day = addZero(String(date.getDate()));
		var hour = date.getHours();
		var minute = addZero(String(date.getMinutes())).substr(0,1);
		
		
		var today = year + "-" + month + "-" + day;
		
		var url = "${ctxPath}/chart/getTimeData.do";
		var param = "dvcId=" + dvcId + 
					"&workDate=" + today;
		
		setInterval(function (){
			var minute = String(new Date().getMinutes());
			if(minute.length!=1){
				minute = minute.substr(1,2);
			};
			
			if(minute==2 && eval("dvcMap" + idx).get("initFlag") || minute==2 && typeof(eval("dvcMap" + idx).get("initFlag")=="undefined")){
				getStatusChart2(dvcId, idx);
				console.log("init")
				eval("dvcMap" + idx).put("initFlag", false);
				eval("dvcMap" + idx).put("currentFlag", true);
			}else if(minute!=2){
				eval("dvcMap" + idx).put("initFlag", true);
			};
		}, 1000 * 10);
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				
				eval("dvcMap" + idx + " = new JqMap();");
				
				if(data==null || data==""){
					eval("dvcMap" + idx).put("noSeries", true);
					getCurrentDvcStatus(dvcId, idx);
					return;
				}else{
					eval("dvcMap" + idx).put("noSeries", false);
				};
	
				 
				 var status = $("#status2_" + idx).highcharts();
				var options = status.options;
					
				var json = data.statusList;
				
				var color = "";
				
				var status = json[0].status;
				if(status=="IN-CYCLE"){
					color = "green"
				}else if(status=="WAIT"){
					color = "yellow";
				}else if(status=="ALARM"){
					color = "red";
				}else if(status=="NO-CONNECTION"){
					color = "gray";
				};
				
				var blank;
				var f_Hour = json[0].startDateTime.substr(11,2);
				var f_Minute = json[0].startDateTime.substr(14,2);
				
				var startHour = 20;
				var startMinute = 3;
				
				var startN = 0;
				
				spdLoadPoint = [];
				spdOverridePoint = [];
				
				
				if(f_Hour==startHour && f_Minute==(startMinute*10)){
					options.series.push({
						data : [ {
							y : Number(20),
							segmentColor : color
						} ],
					});
				}else{
					if(f_Hour>=20){
						startN = (((f_Hour*60) + Number(f_Minute)) - ((startHour*60) + (startMinute*10)))/2; 
					}else{
						startN = ((24*60) - ((startHour*60) + (startMinute*10)))/2;
						startN += (f_Hour*60/2) + (f_Minute/2);
					};
					
					options.series.push({
						data : [ {
							y : Number(20),
							segmentColor : "gray"
						} ],
					});
						
					for(var i = 0; i < startN-1; i++){
						options.series[0].data.push({
							y : Number(20),
							segmentColor : "gray"
						});
						spdLoadPoint.push(Number(0));
						spdOverridePoint.push(Number(0));
					};
				};
				
				
				
				
				$(json).each(function(idx, data){
					if(data.status=="IN-CYCLE"){
						color = "green"
					}else if(data.status=="WAIT"){
						color = "yellow";
					}else if(data.status=="ALARM"){
						color = "red";
					}else if(data.status=="NO-CONNECTION"){
						color = "gray";
					};
					options.series[0].data.push({
						y : Number(20),
						segmentColor : color
					});
				});
				
				for(var i = 0; i < 719-(json.length+startN); i++){
					options.series[0].data.push({
						y : Number(20),
						segmentColor : "rgba(0,0,0,0)"
					});
				};
				
				
				
				 
				status = new Highcharts.Chart(options);
				//getCurrentDvcStatus(dvcId, idx);
			}
		});
	};
	
	function getCurrentDvcStatus(dvcId, idx){
		var url = "${ctxPath}/chart/getCurrentDvcData.do";
		var param = "dvcId=" + dvcId;
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var chartStatus = data.chartStatus;
				var name = data.name;
				
				var status = $("#status2_" + idx).highcharts();
				var options = status.options;

				if(eval("dvcMap" + idx).get("currentFlag") || typeof(eval("dvcMap" + idx).get("currentFlag"))=="undefined"){
					if(eval("dvcMap" + idx).get("noSeries")){
		      			options.series = [];
		      			options.series.push({
					        data: [{
					        		y : Number(200),
					        		segmentColor : slctChartColor(chartStatus) 
					        	}],
					    });
		      		}else{
		      			options.series[0].data.push({
			        		y : Number(200),
			        		segmentColor : slctChartColor(chartStatus)
			  			});		      			
		      		};

		      		var now = options.series[0].data.length;
					//var blank = 144 - now;
					var blank = 719 - now;
					
					for(var i = 0; i <= blank; i++){
						options.series[0].data.push({
			        		y : Number(200),
			        		segmentColor : "rgba(0,0,0,0.0)"
			  			});	
					};
					
					status = new Highcharts.Chart(options);
					eval("dvcMap" + idx).put("currentFlag", false);
				};
				setTimeout(function (){
					getCurrentDvcStatus(dvcId, idx);
				}, 3000);
				$("#dvcName" + idx).html(name);
			}
		});	
	};
	
	function setStartTime(){
		var url = "${ctxPath}/chart/getStartTime.do"
		
		$.ajax({
			url :url,
			dataType :"text",
			type : "post",
			success : function(data){
				startTime = Number(data);
				for(var i = 0; i<=24; i++){
					if(i+startTime>24){
						startTime -= 24;
					};
					
					timeLabel.push(i+startTime);
					for(var j = 0; j < 5; j++){
						timeLabel.push(0);	
					};
				};					
			}
		});
	};
	
	var timeLabel = [];
	
	function slctChartColor(status){
		var color;
		if(status.toLowerCase()=="in-cycle"){
			color = colors[0];
		}else if(status.toLowerCase()=="wait"){
			color = colors[1];
		}else if(status.toLowerCase()=="alarm"){
			color = colors[2];
		}else if(status.toLowerCase()=="no-connection"){
			color = colors[3];
		};
		
		return color;
	};
	
	var startTimeLabel = new Array();
	
	var startHour = 20;
	var startMinute = 3;
	
	var colors;
	
var startTimeLabel = new Array();
	
	var startHour = 20;
	var startMinute = 3;
	
	var colors;
	
	function drawBarChart2(id, name){
		var fontColor = "white;"
		if(name=="NB13" || name=="NB14W"){
			fontColor = "black";
		}
		
		var m0 = "",
		m02 = "",
		m04 = "",
		m06 = "",
		m08 = "",
		
		m1 = "";
		m12 = "",
		m14 = "",
		m16 = "",
		m18 = "",
		
		m2 = "";
		m22 = "",
		m24 = "",
		m26 = "",
		m28 = "",
		
		m3 = "";
		m32 = "",
		m34 = "",
		m36 = "",
		m38 = "",
		
		m4 = "";
		m42 = "",
		m44 = "",
		m46 = "",
		m48 = "",
		
		m5 = "";
		m52 = "",
		m54 = "",
		m56 = "",
		m58 = "";
	
	var n = Number(startHour);
	if(startMinute!=0) n+=1;
	
	for(var i = 0, j = n ; i < 24; i++, j++){
		eval("m" + startMinute + "=" + j);
		
		startTimeLabel.push(m0);
		startTimeLabel.push(m02);
		startTimeLabel.push(m04);
		startTimeLabel.push(m06);
		startTimeLabel.push(m08);
		
		startTimeLabel.push(m1);
		startTimeLabel.push(m12);
		startTimeLabel.push(m14);
		startTimeLabel.push(m16);
		startTimeLabel.push(m18);
		
		startTimeLabel.push(m2);
		startTimeLabel.push(m22);
		startTimeLabel.push(m24);
		startTimeLabel.push(m26);
		startTimeLabel.push(m28);
		
		startTimeLabel.push(m3);
		startTimeLabel.push(m32);
		startTimeLabel.push(m34);
		startTimeLabel.push(m36);
		startTimeLabel.push(m38);
		
		startTimeLabel.push(m4);
		startTimeLabel.push(m42);
		startTimeLabel.push(m44);
		startTimeLabel.push(m46);
		startTimeLabel.push(m48);
		
		startTimeLabel.push(m5);
		startTimeLabel.push(m52);
		startTimeLabel.push(m54);
		startTimeLabel.push(m56);
		startTimeLabel.push(m58);
		
		if(j==24){ j = 0}
	};
	
		
		var perShapeGradient = {
	            x1: 0,
	            y1: 0,
	            x2: 1,
	            y2: 0
	        };
	        colors = Highcharts.getOptions().colors;
	        colors = [{
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#148F01'],
	                [1, '#1ABB02']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#C7C402'],
	                [1, '#F5F104']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#ff0000'],
	                [1, '#FF2626']
	                ]
	           },{
		            linearGradient: perShapeGradient,
		            stops: [
		                [0, '#8c9089'],
		                [1, '#A9ADA6']
		                ]}
	        ]
	         
		var options = {
			chart : {
				type : 'coloredarea',
				backgroundColor : 'rgba(255, 255, 255, 0)',
				height : $("#mainTable").height()*0.9/11, 
				marginTop: -60,
				marginBottom: 25
			},
			credits : false,
			exporting: false,
			title : {
				text :name,
				align :"left",
				y:10,
				style : {
					color : "black",
					fontSize: getElSize(40) + "px",
					fontWeight: 'bold'
				}
			},
			yAxis : {
				labels : {
					enabled : false,
				},
				title : {
					text : false,
	                rotation: 0,
	                "textAlign": 'right',
	                x:100,
	                y: -getElSize(10),
					style : {
						color : "black",
						fontSize: getElSize(20) + "px",
						fontWeight: 'bold'
					}
				},
			},
			xAxis:{
		           categories:startTimeLabel,
		            labels:{
		            	step: 1,
						formatter : function() {
							var val = this.value

							return val;
						},
			                        style :{
			    	                	color : fontColor,
			    	                	fontSize : "9px"
			    	                },
		            }
		        },
		       
			tooltip : {
				headerFormat : "",
				style : {
					fontSize : '10px',
				},
				enabled : false
			}, 
			plotOptions: {
			    line: {
			        marker: {
			            enabled: false
			        }
			    },
			    series : {
			    	animation : false
			    }
			},
			legend : {
				enabled : false
			},
			series: []
		}

	   	$('#' + id).highcharts(options);
	};
	
	function nextPage(){
		if($("svg").length<(max_row)) return;
		c_page++;
		if(c_page>1) $("#prev").addClass("enablePointer");
		getDvcIdList()
	};

	var dateArray = []
	function prevPage(){
		if(c_page<=1) return;
		c_page--;
		if(c_page==1) $("#prev").removeClass("enablePointer");
		getDvcIdList();
	}
	
	</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<div id="bannerDiv">
	<Center>
		<input type="text" id="banner" size="100"><input type="color" id="color"><br>
		<button onclick="addBanner();">적용</button>
		<button onclick="cancelBanner();">미적용</button>
	</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" style="display: none">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right' style="display: none">
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/monitor_left.png" class='menu_left'  style="display: none">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/blue_right.png" class='menu_right' style="display: none">
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left' style="display: none">
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="mainTable" style="border-collapse: collapse; width: 100%" >
						<tr>
							<td class="leftTd">
								<div id="status2_0" class="status"></div>
							</td>
							<td>
								<div id="status2_1" class="status"></div>			
							</td>
						</tr>
						<tr>
							<td class="leftTd">
								<div id="status2_2" class="status"></div>
							</td>
							<td>
								<div id="status2_3" class="status"></div>
							</td>
						</tr>
						<tr>
							<td class="leftTd">
								<div id="status2_4" class="status"></div>
							</td>
							<td>
								<div id="status2_5" class="status"></div>
							</td>
						</tr>
						<tr>
							<td class="leftTd">
								<div id="status2_6" class="status"></div>
							</td>
							<td>
								<div id="status2_7" class="status"></div>
							</td>
						</tr>	
						<tr>		
							<td class="leftTd">
								<div id="status2_8" class="status"></div>
							</td>
							<td>
								<div id="status2_9" class="status"></div>
							</td>
						<tr>	
							<td class="leftTd">
								<div id="status2_10" class="status"></div>
							</td>
							<td>
								<div id="status2_11" class="status"></div>
							</td>
						</tr>	
						<tr>
							<td class="leftTd">
								<div id="status2_12" class="status"></div>
							</td>
							<Td>
								<div id="status2_13" class="status"></div>
							</Td>
						</tr>
						<tr>
							<td class="leftTd">
								<div id="status2_14" class="status"></div>
							</td>
							<td>
								<div id="status2_15" class="status"></div>
							</td>	
						</tr>
						<tr>
							<td class="leftTd">
								<div id="status2_16" class="status"></div>
							</td>
							<td>
								<div id="status2_17" class="status"></div>
							</td>
						</tr>	
						<tr>		
							<Td class="leftTd">
								<div id="status2_18" class="status"></div>
							</Td>	
							<td>
								<div id="status2_19" class="status"></div>
							</td>
						</tr>
						<Tr>
							<td colspan="2" align="center" style="padding: 0px">
								<img alt="" src="${ctxPath }/images/arrow_left_black.png" id="prev">
								<img alt="" src="${ctxPath }/images/arrow_right_black.png" id="next">
							</td>
						</Tr>	
					</table>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	