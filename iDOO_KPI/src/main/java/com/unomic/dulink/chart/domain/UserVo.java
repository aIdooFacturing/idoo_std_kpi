package com.unomic.dulink.chart.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserVo {
	String name;
	int empCd;
	String passWord;
	
}
